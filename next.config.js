/* eslint-disable */

module.exports = {
  env: {
    baseHP: 50,
    baseHPIncrease: 3,

    domain: 'cero-next.vercel.app',

    contractCTAddress: '',
    contractCBCAddress: '',
    contractCCCAddress: '',
    contractLUAddress: '',
    contractFAddress: '',

    contractCTRopstenAddress: '0xbc0B53A618E7F83BaF30E53c195CD9C44083F936',
    contractCBCRopstenAddress: '0x950655e4d62598E58db24235c925dAcb3Ef783a3',
    contractCCCRopstenAddress: '0x01DF19C61a757d5455036436b332b15bF12f9E74',
    contractLURopstenAddress: '0x0A166132f93d849E71177FDE55b03CFFC2D51dbA',
    contractFRopstenAddress: '0x9b2779C23E64eeD2973CF97d0f78d425361a3dD4',

    contractCTRinkebyAddress: '0xF88Dd1b91555B855be6472F5B495dA99FE9440b7',
    contractCBCRinkebyAddress: '0x93b7Ba0C22977Bdd58935Bd849f3E129579dCedf',
    contractCCCRinkebyAddress: '0x6C3401D71CEd4b4fEFD1033EA5F83e9B3E7e4381',
    contractLURinkebyAddress: '0x8Eb2f4Ad5afA9d181b8bf3c6e2aA9A690A7f1dde',
    contractFRinkebyAddress: '0xea040B6A62C6E2aF5e06C1B9C692d4af21540Bea',

    contractCTVelasTestnetAddress: '0xdcf9acCEA41D16B49EC3aDA7d7f339772d377e8b',
    contractCBCVelasTestnetAddress: '0x010B8A4790B3fa61762cd06c8b476194EDC1dE44',
    contractCCCVelasTestnetAddress: '0xe7deB4238d6AcFEE0B457dfa3f51d2e88a085367',
    contractLUVelasTestnetAddress: '0x055f2cFF24Da8Fb18a2D6c154a4fABF985bCBC8D',
    contractFVelasTestnetAddress: '0x0669f4c129Eb2a38F2fC95b666D9B2b6185c8b8b',

    contractCTABI: [
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "approved",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "Approval",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "indexed": false,
            "internalType": "bool",
            "name": "approved",
            "type": "bool"
          }
        ],
        "name": "ApprovalForAll",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "Transfer",
        "type": "event"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "name": "acceptedToCreateBaseToken",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_newOwner",
            "type": "address"
          }
        ],
        "name": "addOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "approve",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          }
        ],
        "name": "balanceOf",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "baseURI",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "ceroes",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "lvl",
            "type": "uint8"
          },
          {
            "internalType": "uint16",
            "name": "strength",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "protection",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "agility",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "magic",
            "type": "uint16"
          },
          {
            "internalType": "uint64",
            "name": "birthday",
            "type": "uint64"
          },
          {
            "internalType": "uint256",
            "name": "parent1",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "parent2",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "experience",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "isChild",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "getApproved",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          }
        ],
        "name": "isApprovedForAll",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_address",
            "type": "address"
          }
        ],
        "name": "isOwner",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "name",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "name1",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "name2",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "ownerOf",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_oldOwner",
            "type": "address"
          }
        ],
        "name": "removeOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "safeTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          },
          {
            "internalType": "bytes",
            "name": "_data",
            "type": "bytes"
          }
        ],
        "name": "safeTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "operator",
            "type": "address"
          },
          {
            "internalType": "bool",
            "name": "approved",
            "type": "bool"
          }
        ],
        "name": "setApprovalForAll",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "_newName",
            "type": "string"
          }
        ],
        "name": "setName1",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "string",
            "name": "_newName",
            "type": "string"
          }
        ],
        "name": "setName2",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "bytes4",
            "name": "interfaceId",
            "type": "bytes4"
          }
        ],
        "name": "supportsInterface",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "symbol",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "index",
            "type": "uint256"
          }
        ],
        "name": "tokenByIndex",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "index",
            "type": "uint256"
          }
        ],
        "name": "tokenOfOwnerByIndex",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "tokenURI",
        "outputs": [
          {
            "internalType": "string",
            "name": "",
            "type": "string"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "totalSupply",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "from",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "to",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "tokenId",
            "type": "uint256"
          }
        ],
        "name": "transferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_oldOwner",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenId",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "_tokenURI",
            "type": "string"
          }
        ],
        "name": "setTokenURI",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "getTokensCount",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "generateFirstToken",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenOwner",
            "type": "address"
          },
          {
            "internalType": "uint8",
            "name": "_lvl",
            "type": "uint8"
          },
          {
            "internalType": "uint16",
            "name": "_st",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_pr",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ag",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ma",
            "type": "uint16"
          },
          {
            "internalType": "uint256",
            "name": "_parent1",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "_parent2",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "_isChild",
            "type": "bool"
          }
        ],
        "name": "createToken",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_address",
            "type": "address"
          },
          {
            "internalType": "uint256",
            "name": "_tokenCount",
            "type": "uint256"
          }
        ],
        "name": "setAcceptedToCreateBaseToken",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "_newStatus",
            "type": "bool"
          }
        ],
        "name": "updateTokenChildStatus",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "_newExperience",
            "type": "uint256"
          }
        ],
        "name": "updateTokenExperience",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          },
          {
            "internalType": "uint16",
            "name": "_st",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_pr",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ag",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ma",
            "type": "uint16"
          },
          {
            "internalType": "uint8",
            "name": "_lvl",
            "type": "uint8"
          }
        ],
        "name": "updateTokenAfterLevelUp",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          }
        ],
        "name": "getTokenShortDataType1",
        "outputs": [
          {
            "internalType": "uint16",
            "name": "",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "",
            "type": "uint16"
          },
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          },
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          }
        ],
        "name": "getTokenShortDataType2",
        "outputs": [
          {
            "internalType": "uint16",
            "name": "_st",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_pr",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ag",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ma",
            "type": "uint16"
          },
          {
            "internalType": "uint8",
            "name": "_lvl",
            "type": "uint8"
          },
          {
            "internalType": "bool",
            "name": "_isChild",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "_exp",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          }
        ],
        "name": "getTokenShortDataType3",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          },
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "getAllTokens",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint8",
                "name": "lvl",
                "type": "uint8"
              },
              {
                "internalType": "uint16",
                "name": "strength",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "protection",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "agility",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "magic",
                "type": "uint16"
              },
              {
                "internalType": "uint64",
                "name": "birthday",
                "type": "uint64"
              },
              {
                "internalType": "uint256",
                "name": "parent1",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "parent2",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "experience",
                "type": "uint256"
              },
              {
                "internalType": "string",
                "name": "name",
                "type": "string"
              },
              {
                "internalType": "bool",
                "name": "isChild",
                "type": "bool"
              }
            ],
            "internalType": "struct ICeroStruct.Cero[]",
            "name": "_ceroes",
            "type": "tuple[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_owner",
            "type": "address"
          }
        ],
        "name": "getTokensByAddress",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint8",
                "name": "lvl",
                "type": "uint8"
              },
              {
                "internalType": "uint16",
                "name": "strength",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "protection",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "agility",
                "type": "uint16"
              },
              {
                "internalType": "uint16",
                "name": "magic",
                "type": "uint16"
              },
              {
                "internalType": "uint64",
                "name": "birthday",
                "type": "uint64"
              },
              {
                "internalType": "uint256",
                "name": "parent1",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "parent2",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "experience",
                "type": "uint256"
              },
              {
                "internalType": "string",
                "name": "name",
                "type": "string"
              },
              {
                "internalType": "bool",
                "name": "isChild",
                "type": "bool"
              }
            ],
            "internalType": "struct ICeroStruct.Cero[]",
            "name": "_ceroes",
            "type": "tuple[]"
          },
          {
            "internalType": "uint256[]",
            "name": "_ids",
            "type": "uint256[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      }
    ],
    contractCBCABI: [
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenContract",
            "type": "address"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "inputs": [],
        "name": "getBalance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "tokenContract",
        "outputs": [
          {
            "internalType": "contract CeroToken",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "withdrawReward",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenOwner",
            "type": "address"
          }
        ],
        "name": "createBaseCero",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ],
    contractCCCABI: [
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenContract",
            "type": "address"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "newChild",
            "type": "uint256"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "parent1",
            "type": "uint256"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "parent2",
            "type": "uint256"
          }
        ],
        "name": "ChildCreate",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "inputs": [],
        "name": "getBalance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "tokenContract",
        "outputs": [
          {
            "internalType": "contract CeroToken",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "withdrawReward",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_cero1Num",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "_cero2Num",
            "type": "uint256"
          }
        ],
        "name": "createChildCero",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ],
    contractLUABI: [
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenContract",
            "type": "address"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "inputs": [],
        "name": "getBalance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "pointForDistribution",
        "outputs": [
          {
            "internalType": "uint8",
            "name": "",
            "type": "uint8"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "tokenContract",
        "outputs": [
          {
            "internalType": "contract CeroToken",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "withdrawReward",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_ceroNum",
            "type": "uint256"
          },
          {
            "internalType": "uint16",
            "name": "_st",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_pr",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ag",
            "type": "uint16"
          },
          {
            "internalType": "uint16",
            "name": "_ma",
            "type": "uint16"
          }
        ],
        "name": "levelUp",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ],
    contractFABI: [
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "_tokenContract",
            "type": "address"
          },
          {
            "internalType": "address",
            "name": "_createBaseCeroContract",
            "type": "address"
          }
        ],
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "attacker",
            "type": "uint256"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "defender",
            "type": "uint256"
          },
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "winner",
            "type": "uint256"
          },
          {
            "indexed": false,
            "internalType": "bool",
            "name": "isNewTokenAccepted",
            "type": "bool"
          },
          {
            "indexed": false,
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          }
        ],
        "name": "FightResult",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "address",
            "name": "previousOwner",
            "type": "address"
          },
          {
            "indexed": true,
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
      },
      {
        "inputs": [],
        "name": "baseHP",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "baseHPIncrease",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "createBaseCeroContract",
        "outputs": [
          {
            "internalType": "contract CreateBaseCero",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "fightLogs",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "attacker",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "defender",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "winner",
            "type": "uint256"
          },
          {
            "internalType": "uint64",
            "name": "time",
            "type": "uint64"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "getBalance",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "tokenContract",
        "outputs": [
          {
            "internalType": "contract CeroToken",
            "name": "",
            "type": "address"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "address",
            "name": "newOwner",
            "type": "address"
          }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [],
        "name": "withdrawReward",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_attackerNum",
            "type": "uint256"
          }
        ],
        "name": "fight",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_tokenNum",
            "type": "uint256"
          }
        ],
        "name": "getLogsForToken",
        "outputs": [
          {
            "components": [
              {
                "internalType": "uint256",
                "name": "attacker",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "defender",
                "type": "uint256"
              },
              {
                "internalType": "uint256",
                "name": "winner",
                "type": "uint256"
              },
              {
                "internalType": "uint64",
                "name": "time",
                "type": "uint64"
              }
            ],
            "internalType": "struct Fight.FightLog[]",
            "name": "_logs",
            "type": "tuple[]"
          }
        ],
        "stateMutability": "view",
        "type": "function"
      }
    ],
  },
};
