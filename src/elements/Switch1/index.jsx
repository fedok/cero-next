import Switch from '@material-ui/core/Switch';
import PropTypes from 'prop-types';
import cl from './style.module.scss';

export default function Switch1({ name, checked, onChange }) {
  return (
    <div className={cl.container}>
      <Switch
        checked={checked}
        onChange={() => onChange(!checked)}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
      <style global jsx> {`
        .MuiSwitch-root .MuiSwitch-track {
          opacity: 0.3;
          background-color: #FFFFFF;
        }
        .MuiSwitch-root .MuiSwitch-colorPrimary.Mui-checked + .MuiSwitch-track {
          opacity: 1;
          background-color: #7BB0FF;
        }
        .MuiSwitch-root .MuiSwitch-colorPrimary.Mui-checked {
          color: #ffffff;
        }
      `}
      </style>
      <span>{name}</span>
    </div>
  );
}

Switch1.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};
