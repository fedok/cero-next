/* eslint-disable max-len */

import { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Translator from 'handlers/translator';
import cl from './style.module.scss';

export default function PageInfo({ headerDef, descriptionDef }) {
  const store = useSelector((state) => state);
  const router = useRouter();
  const trans = new Translator(store.language, 'header');

  const pagesInfo = [
    {
      link: '/collection',
      header: trans.t('Collection'),
      description: `${trans.t('headerCollection1')} ${store.account.souls > 0 ? `<b>${trans.t('headerCollection2')} ${store.account.souls} ${trans.t('headerCollection3')}</b>` : ''}`,
    },
    {
      link: '/creation',
      header: trans.t('Creation'),
      description: trans.t('On this page you can create a new cero based on the ones you already have.'),
    },
    {
      link: '/account',
      header: trans.t('Account'),
      description: trans.t('Here you can find information about your current wallet, balance and network.'),
    },
    {
      link: '/faq',
      header: trans.t('FAQ'),
      description: trans.t('Here you will find answers to frequently asked questions.'),
    },
    {
      link: '/rating',
      header: trans.t('Rating'),
      description: trans.t('A list of all existing ceroes. You can sort by one of the characteristics or search by name or number.'),
    },
    {
      link: '/cero',
      header: '',
      description: trans.t('This is where all the important information about your cero can be found: number, level, characteristic, combat stats and other important information.'),
    },
    {
      link: '/level-up',
      header: trans.t('Level up'),
      description: `${trans.t('Here you can level up your cero, remember that you have')} <green>${store.contract.pointsForDistribution} ${trans.t('points')}</green> ${trans.t('available for distribution. Strength and magic affect a character\'s attack, defence affects the ability to block damage and health, and agility affects the ability to dodge a hit.')}`,
    },
  ];

  const renderHeader = () => {
    let header = headerDef;

    if (header === '') {
      pagesInfo.some((pageInfo) => {
        if (router.pathname.includes(pageInfo.link)) {
          header = pageInfo.header;
          return true;
        }
        return false;
      });
    }

    document.getElementById('page-info_header').innerHTML = header;
  };

  const renderDescription = () => {
    let description = descriptionDef;

    if (description === '') {
      pagesInfo.forEach((pageInfo) => {
        if (router.pathname.includes(pageInfo.link)) {
          description = pageInfo.description;
          return true;
        }
        return false;
      });
    }

    document.getElementById('page-info_description').innerHTML = description;
  };

  useEffect(() => {
    renderHeader();
    renderDescription();
  });

  return (
    <section className={cl.pageDescription}>
      <Grid container spacing={4}>
        <Grid item sm={12} md={5}>
          <h1 id="page-info_header">Collection</h1>
        </Grid>
        <Grid className={cl.description} item sm={12} md={7} alignItems="center" container>
          <img src="/images/stars-1.png" alt="Stars" className={cl.image} />
          <p id="page-info_description" />
        </Grid>
      </Grid>
    </section>
  );
}

PageInfo.propTypes = {
  headerDef: PropTypes.string,
  descriptionDef: PropTypes.string,
};

PageInfo.defaultProps = {
  headerDef: '',
  descriptionDef: '',
};
