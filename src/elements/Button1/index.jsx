import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import cl from './style.module.scss';

export default function Button1({ name, type, inactive, onClick }) {
  return (
    <Button
      className={`${cl.button} ${cl.styles} ${cl[type]} ${inactive === true ? cl.inactive : ''}`}
      variant="contained"
      onClick={inactive === true ? () => {} : onClick}
    >
      {name}
    </Button>
  );
}

Button1.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  onClick: PropTypes.func,
  inactive: PropTypes.bool,
};

Button1.defaultProps = {
  type: 'type-1',
  inactive: false,
  onClick: () => {},
};
