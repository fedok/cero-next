import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import cl from './style.module.scss';

export default function InputBase({ label, variant, onChange }) {
  return (
    <TextField
      className={`${cl.input} ${cl.styles}`}
      label={label}
      variant={variant}
      onChange={onChange}
      InputProps={{
        classes: {
          focused: cl.focused,
        },
      }}
      InputLabelProps={{
        classes: {
          focused: cl.focusedLabel,
        },
      }}
    />
  );
}

InputBase.propTypes = {
  label: PropTypes.string.isRequired,
  variant: PropTypes.string,
  onChange: PropTypes.func,
};

InputBase.defaultProps = {
  variant: 'outlined',
  onChange: () => {},
};
