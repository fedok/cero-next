import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import cl from './style.module.scss';

export default function ContainerCustom({ children }) {
  return <Container maxWidth="lg" className={cl.container}>{children}</Container>;
}

ContainerCustom.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
};
