import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { tokenNumberToString } from 'handlers/token.handler';
import Translator from 'handlers/translator';
import { useSelector } from 'react-redux';
import cl from './style.module.scss';

export default function CeroContent({ token }) {
  const router = useRouter();
  const store = useSelector((state) => state);
  const trans = new Translator(store.language, 'token');

  useEffect(() => {
    const bars = document.querySelectorAll(`.exp-bar-${token.id}-calc`);
    if (bars.length === 0) return;

    bars.forEach((bar) => {
      if (token.exp === token.expToUp) {
        bar.style.width = 'calc(100% + 2px)';
      } else {
        bar.style.width = `${(token.exp * 100) / token.expToUp}%`;
      }
    });
  }, [token.exp]);

  const onClickLevelUp = (e, num) => {
    e.stopPropagation();
    router.push(`/level-up/${num}`);
  };

  const name1 = trans.t(token.name.split(' ')[0]);
  const name2 = trans.t(token.name.split(' ')[1]);

  return (
    <>
      <div className={cl.numberLevelUpCollectible}>
        <span>{tokenNumberToString(token.id)}</span>
        <a onClick={(e) => onClickLevelUp(e, Number(token.id))}>
          <span>LEVEL UP</span>
          <img src="/images/double-arrow.svg" alt="double-arrow" />
        </a>
        <span>{trans.t('COLLECTIBLE')}</span>
      </div>
      <div className={cl.tokenImage}>
        <img src={token.img} alt={token.name} />
      </div>
      <div className={cl.name}>
        <span>{trans.t('Name')}</span>
        <span>{`${name1} ${name2}`}</span>
      </div>
      <div className={`${cl.experience} ${token.exp === token.expToUp ? cl.maxExperience : ''}`}>
        <div>
          <span>{trans.t('Experience')}</span>
          <span>{`${token.exp} / ${token.expToUp}`}</span>
        </div>
        <div>
          <div
            // id={`exp-bar-${token.id}-calc`}
            className={`exp-bar-${token.id}-calc`}
          />
        </div>
      </div>
    </>
  );
}

CeroContent.propTypes = {
  token: PropTypes.object.isRequired,
};
