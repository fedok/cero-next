import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Translator from 'handlers/translator';
import cl from './style.module.scss';
import CeroContent from '../CeroContent';

export default function CeroFullCard({ token, showLvlUp, renderRight }) {
  const store = useSelector((state) => state);
  const trans = new Translator(store.language);
  const transT = new Translator(store.language, 'token');

  const className = `${token.class[0]}-${token.img[token.img.length - 5]}`;

  useEffect(() => {
    const bars = document.querySelectorAll(`.exp-bar-${token.id}-calc`);
    if (bars.length === 0) return;

    bars.forEach((bar) => {
      if (token.exp === token.expToUp) {
        bar.style.width = 'calc(100% + 2px)';
      } else {
        bar.style.width = `${(token.exp * 100) / token.expToUp}%`;
      }
    });
  }, []);

  const renderRightSection = () => {
    if (renderRight === false) return '';

    return (
      <div className={cl.rightSection}>
        <div>
          <div>
            <span>{trans.t('Level')}</span>
            <span>{token.level}</span>
          </div>
          <div>
            <span>{trans.t('Health')}</span>
            <span>{token.health}</span>
          </div>
        </div>
        <div>
          <div>
            <span>{trans.t('Strength')}</span>
            <span>{token.strength}</span>
          </div>
          <div>
            <span>{trans.t('Protection')}</span>
            <span>{token.protection}</span>
          </div>
        </div>
        <div>
          <div>
            <span>{trans.t('Agility')}</span>
            <span>{token.agility}</span>
          </div>
          <div>
            <span>{trans.t('Magic')}</span>
            <span>{token.magic}</span>
          </div>
        </div>
      </div>
    );
  };

  return (
    // eslint-disable-next-line max-len
    <div className={`${cl.container} ${cl[className]}
      ${token.exp === token.expToUp && showLvlUp ? cl.lvlUp : ''}
      ${token.isChild ? '' : cl.collectible}`}
    >
      <div className={cl.leftSection}>
        <CeroContent token={token} />
        <div>
          <span>{transT.t('Class')}</span>
          <span>{transT.t(token.class)}</span>
        </div>
        <div>
          <span>{transT.t(token.description)}</span>
        </div>
      </div>
      {renderRightSection()}
    </div>
  );
}

CeroFullCard.propTypes = {
  token: PropTypes.object.isRequired,
  renderRight: PropTypes.bool,
  showLvlUp: PropTypes.bool,
};

CeroFullCard.defaultProps = {
  renderRight: true,
  showLvlUp: true,
};
