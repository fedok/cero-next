import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import cl from './style.module.scss';
import CeroContent from '../CeroContent';
import Translator from 'handlers/translator';
import { useSelector } from 'react-redux';

export default function CeroShortCard({ token, onClick, showLvlUp }) {
  const router = useRouter();
  const store = useSelector((state) => state);
  const trans = new Translator(store.language, 'token');

  const onclickLoc = onClick === null ? () => router.push(`/cero/${Number(token.id)}`) : onClick;

  return (
    <div
      className={`${cl.container} ${token.exp === token.expToUp && showLvlUp ? cl.lvlUp : ''} 
      ${token.isChild ? '' : cl.collectible}`}
      onClick={onclickLoc}
    >
      <CeroContent token={token} />
      <div>
        <div>
          <span>{trans.t('Class')}</span>
          <span>{trans.t(token.class)}</span>
        </div>
        <div>
          <span>{token.level}</span>
          <span>lvl</span>
        </div>
      </div>
    </div>
  );
}

CeroShortCard.propTypes = {
  token: PropTypes.object.isRequired,
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.oneOf([null])]),
  showLvlUp: PropTypes.bool,
};

CeroShortCard.defaultProps = {
  onClick: null,
  showLvlUp: true,
};
