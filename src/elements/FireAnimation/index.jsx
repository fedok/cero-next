import cl from './style.module.scss';

export default function FireAnimation() {
  return (
    <div className={cl.fire}>
      <div className={cl.flames}>
        <div className={cl.flame} />
        <div className={cl.flame} />
        <div className={cl.flame} />
        <div className={cl.flame} />
      </div>
      <div className={cl.logs} />
    </div>
  );
}
