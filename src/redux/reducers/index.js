import { combineReducers } from 'redux';

import language from 'redux/reducers/src/language/reducer';
import modal from 'redux/reducers/src/modal/reducer';
import account from 'redux/reducers/src/account/reducer';
import web3 from 'redux/reducers/src/web3/reducer';
import contract from 'redux/reducers/src/contract/reducer';

const RootReducer = combineReducers({
  language,
  modal,
  account,
  web3,
  contract,
});

export default RootReducer;
