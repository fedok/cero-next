import { MODAL_CHOICE_CERO } from 'components/modals/ModalLayout';

export const MODAL_SET_ACTIVE_WINDOW = 'MODAL_CHANGE_ACTIVE_WINDOW';
export const MODAL_SET_NEXT_ACTIVE_WINDOW = 'MODAL_SET_NEXT_ACTIVE_WINDOW';
export const MODAL_CLOSE = 'MODAL_CLOSE';
export const MODAL_SET_TOKEN1 = 'MODAL_SET_TOKEN1';
export const MODAL_SET_TOKEN2 = 'MODAL_SET_TOKEN2';
export const MODAL_SET_ALERT_CONTENT = 'MODAL_SET_ALERT_CONTENT';
export const MODAL_SET_CONTENT = 'MODAL_SET_CONTENT';
export const MODAL_SET_IS_ATTACKER_WIN = 'MODAL_SET_IS_ATTACKER_WIN';
export const MODAL_SET_IS_NEW_SOUL_PART = 'MODAL_SET_IS_NEW_SOUL_PART';
export const MODAL_SET_CHILD_TOKEN = 'MODAL_SET_CHILD_TOKEN';

export const modalSetActiveWindow = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_ACTIVE_WINDOW, payload });
};

export const modalSetNextActiveWindow = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_NEXT_ACTIVE_WINDOW, payload });
};

export const modalClose = (dispatch) => {
  dispatch({ type: MODAL_CLOSE });
};

export const modalSetToken1 = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_TOKEN1, payload });
};

export const modalSetToken2 = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_TOKEN2, payload });
};

export const modalSetAlertContent = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_ALERT_CONTENT, payload });
};

export const modalSetContent = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_CONTENT, payload });
};

export const modalSetIsAttackerWin = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_IS_ATTACKER_WIN, payload });
};

export const modalSetIsNewSoulPart = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_IS_NEW_SOUL_PART, payload });
};

export const modalSetChildToken = (payload) => (dispatch) => {
  dispatch({ type: MODAL_SET_CHILD_TOKEN, payload });
};

export const showAlertWindow = (content) => (dispatch) => {
  dispatch(modalSetAlertContent(content));
  dispatch(modalSetActiveWindow('modal-alert'));
};

export const showChoiceTokenWindow = (content) => (dispatch) => {
  dispatch(modalSetContent(content));
  dispatch(modalSetActiveWindow(MODAL_CHOICE_CERO));
};
