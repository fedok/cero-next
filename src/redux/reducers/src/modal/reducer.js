import * as types from './action';

const initialState = {
  isActive: false,
  activeWindow: '',
  nextActiveWindow: '',
  token1: null,
  token2: null,
  alertContent: { message: '', btn: '', btnCallback: null },
  content: { title: '', btn: '' },
  isAttackerWin: null,
  isNewSoulPart: null,
  childToken: null,
};

export default function index(state = initialState, action) {
  switch (action.type) {
    case types.MODAL_CLOSE:
      return { ...state, isActive: false };
    case types.MODAL_SET_ACTIVE_WINDOW:
      return { ...state, activeWindow: action.payload, isActive: true };
    case types.MODAL_SET_NEXT_ACTIVE_WINDOW:
      return { ...state, nextActiveWindow: action.payload };
    case types.MODAL_SET_TOKEN1:
      return { ...state, token1: action.payload };
    case types.MODAL_SET_TOKEN2:
      return { ...state, token2: action.payload };
    case types.MODAL_SET_ALERT_CONTENT:
      return { ...state, alertContent: action.payload };
    case types.MODAL_SET_CONTENT:
      return { ...state, content: action.payload };
    case types.MODAL_SET_IS_ATTACKER_WIN:
      return { ...state, isAttackerWin: action.payload };
    case types.MODAL_SET_IS_NEW_SOUL_PART:
      return { ...state, isNewSoulPart: action.payload };
    case types.MODAL_SET_CHILD_TOKEN:
      return { ...state, childToken: action.payload };
    default:
      return state;
  }
}
