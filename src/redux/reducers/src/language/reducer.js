import * as types from './action';

const initialState = 'en';

export default function index(state = initialState, action) {
  switch (action.type) {
    case types.LANG_CHANGE:
      return action.payload;
    default:
      return state;
  }
}
