export const LANG_CHANGE = 'LANG_CHANGE';

export const langChange = (payload) => (dispatch) => {
  dispatch({ type: LANG_CHANGE, payload });
};
