import * as types from './action';

const initialState = {
  account: '',
  network: '',
  balance: 0,
  isConnected: false,
  isContractInfoLoaded: false,
  gasPrice: 0,
};

export default function index(state = initialState, action) {
  switch (action.type) {
    case types.WEB3_ACCOUNT_CHANGE:
      return { ...state, account: action.payload };
    case types.WEB3_BALANCE_CHANGE:
      return { ...state, balance: action.payload };
    case types.WEB3_NETWORK_CHANGE:
      return { ...state, network: action.payload };
    case types.WEB3_SET_IS_CONNECTED:
      return { ...state, isConnected: action.payload };
    case types.WEB3_SET_IS_CONTRACT_INFO_LOADED:
      return { ...state, isContractInfoLoaded: action.payload };
    case types.WEB3_GAS_PRICE_CHANGED:
      return { ...state, gasPrice: action.payload };
    default:
      return state;
  }
}
