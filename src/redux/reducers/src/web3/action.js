export const WEB3_ACCOUNT_CHANGE = 'WEB3_ACCOUNT_CHANGE';
export const WEB3_NETWORK_CHANGE = 'WEB3_NETWORK_CHANGE';
export const WEB3_BALANCE_CHANGE = 'WEB3_BALANCE_CHANGE';
export const WEB3_SET_IS_CONNECTED = 'WEB3_SET_IS_CONNECTED';
export const WEB3_SET_IS_CONTRACT_INFO_LOADED = 'WEB3_SET_IS_CONTRACT_INFO_LOADED';
export const WEB3_GAS_PRICE_CHANGED = 'WEB3_GAS_PRICE_CHANGED';

export const web3AccountChange = (payload) => (dispatch) => {
  dispatch({ type: WEB3_ACCOUNT_CHANGE, payload });
};

export const web3NetworkChange = (payload) => (dispatch) => {
  dispatch({ type: WEB3_NETWORK_CHANGE, payload });
};

export const web3BalanceChange = (payload) => (dispatch) => {
  dispatch({ type: WEB3_BALANCE_CHANGE, payload });
};

export const web3SetIsConnected = (payload) => (dispatch) => {
  dispatch({ type: WEB3_SET_IS_CONNECTED, payload });
};

export const web3SetIsContractInfoLoaded = (payload) => (dispatch) => {
  dispatch({ type: WEB3_SET_IS_CONTRACT_INFO_LOADED, payload });
};

export const web3GasPriceChanged = (payload) => (dispatch) => {
  dispatch({ type: WEB3_GAS_PRICE_CHANGED, payload });
};
