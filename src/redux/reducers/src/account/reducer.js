import * as types from './action';

const initialState = {
  isContractOwner: null,
  soulLoadTime: 0,
  souls: 0,
};

export default function index(state = initialState, action) {
  switch (action.type) {
    case types.ACCOUNT_SOUL_UPDATE:
      return { ...state, soulLoadTime: new Date().getTime(), souls: action.payload };
    case types.ACCOUNT_SET_IS_CONTRACT_OWNER:
      return { ...state, isContractOwner: action.payload };
    default:
      return state;
  }
}
