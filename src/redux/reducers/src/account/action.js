export const ACCOUNT_SET_IS_CONTRACT_OWNER = 'WEB3_SET_IS_CONTRACT_OWNER';
export const ACCOUNT_SOUL_UPDATE = 'ACCOUNT_SOUL_UPDATE';

export const accountSetIsContractOwner = (payload) => (dispatch) => {
  dispatch({ type: ACCOUNT_SET_IS_CONTRACT_OWNER, payload });
};

export const accountSoulsUpdate = (payload) => (dispatch) => {
  dispatch({ type: ACCOUNT_SOUL_UPDATE, payload });
};
