import * as types from './action';

const initialState = {
  owner: '',
  tokensLoadTime: 0,
  tokens: [],
  fightLogs: {},
  pointsForDistribution: 0,
  baseHp: 0,
  baseHpIncrease: 0,
  fee: 0,
};

export default function index(state = initialState, action) {
  switch (action.type) {
    case types.CONTRACT_SET_OWNER:
      return { ...state, owner: action.payload };
    case types.CONTRACT_SET_POINTS_FOR_DISTRIBUTION:
      return { ...state, pointsForDistribution: action.payload };
    case types.CONTRACT_ADD_TOKENS:
      return { ...state, tokens: action.payload, tokensLoadTime: new Date().getTime() };
    case types.CONTRACT_SET_BASE_HP:
      return { ...state, baseHp: action.payload };
    case types.CONTRACT_SET_BASE_HP_INCREASE:
      return { ...state, baseHpIncrease: action.payload };
    case types.CONTRACT_SET_FEE:
      return { ...state, fee: action.payload };
    case types.CONTRACT_ADD_FIGHT_LOGS:
      return { ...state, fightLogs: action.payload };
    default:
      return state;
  }
}
