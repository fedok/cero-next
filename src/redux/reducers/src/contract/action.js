export const CONTRACT_SET_OWNER = 'CONTRACT_SET_OWNER';
export const CONTRACT_SET_POINTS_FOR_DISTRIBUTION = 'CONTRACT_SET_POINTS_FOR_DISTRIBUTION';
export const CONTRACT_ADD_TOKENS = 'CONTRACT_ADD_TOKENS';
export const CONTRACT_SET_BASE_HP = 'CONTRACT_SET_BASE_HP';
export const CONTRACT_SET_BASE_HP_INCREASE = 'CONTRACT_SET_BASE_HP_INCREASE';
export const CONTRACT_SET_FEE = 'CONTRACT_SET_FEE';
export const CONTRACT_ADD_FIGHT_LOGS = 'CONTRACT_ADD_FIGHT_LOGS';

export const contractSetOwner = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_SET_OWNER, payload });
};

export const contractSetPointsForDistribution = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_SET_POINTS_FOR_DISTRIBUTION, payload });
};

export const contractAddTokens = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_ADD_TOKENS, payload });
};

export const contractAddNewTokens = (tokens) => (dispatch, useState) => {
  const store = useState();
  const oldTokens = store.contract.tokens;

  const newTokens = [...oldTokens, ...tokens];
  dispatch({ type: CONTRACT_ADD_TOKENS, payload: newTokens });
};

export const contractAddToken = (newToken) => (dispatch, useState) => {
  const store = useState();
  const oldTokens = store.contract.tokens;

  let isUpdated = false;
  let newTokens = oldTokens.map((oldToken) => {
      if (oldToken.id === newToken.id) {
        isUpdated = true;
        return newToken;
      }
      return oldToken;
  });

  if (!isUpdated) {
    newTokens = [...newTokens, newToken];
  }

  dispatch(contractAddTokens(newTokens));
};

export const contractSetBaseHP = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_SET_BASE_HP, payload });
};

export const contractSetBaseHPIncrease = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_SET_BASE_HP_INCREASE, payload });
};

export const contractSetFee = (payload) => (dispatch) => {
  dispatch({ type: CONTRACT_SET_FEE, payload });
};

export const contractAddFightLog = (tokenNum, newLogs) => (dispatch, useState) => {
  const store = useState();
  const oldLogs = store.contract.fightLogs;

  const newLogsLoc = { ...oldLogs };
  newLogsLoc[tokenNum] = newLogs;

  dispatch({ type: CONTRACT_ADD_FIGHT_LOGS, payload: newLogsLoc });
};

export const contractAddFightLogs = (newLogs) => (dispatch, useState) => {
  const store = useState();
  const oldLogs = store.contract.fightLogs;

  const newLogsLoc = { ...oldLogs, ...newLogs };

  dispatch({ type: CONTRACT_ADD_FIGHT_LOGS, payload: newLogsLoc });
};
