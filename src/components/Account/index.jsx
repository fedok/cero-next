import Grid from '@material-ui/core/Grid';
import PageInfo from 'elements/PageInfo';
import { useSelector } from 'react-redux';
import Translator from 'handlers/translator';
import Head from 'next/head';
import Layout from '../Layout';
import cl from './style.module.scss';

export default function Account() {
  const store = useSelector((state) => state);
  const trans = new Translator(store.language);

  return (
    <Layout>
      <Head><title>Cero | Account</title></Head>
      <PageInfo />
      <Grid container spacing={4} className={cl.container}>
        <Grid item xs={12}>
          <div>
            <p>{trans.t('Wallet')}</p>
            <p>{store.web3.account}</p>
          </div>
          <div>
            <p>{trans.t('Balance')}</p>
            <p>{store.web3.balance} ETH</p>
          </div>
          <div>
            <p>{trans.t('Network')}</p>
            <p>{store.web3.network}</p>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}
