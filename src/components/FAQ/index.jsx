/* eslint-disable max-len, react/no-danger */

import Grid from '@material-ui/core/Grid';
import PageInfo from 'elements/PageInfo';
import ContainerCustom from 'elements/Container';
import { useSelector } from 'react-redux';
import Translator from 'handlers/translator';
import Head from 'next/head';
import cl from './style.module.scss';
import Header from '../Layout/Header';
import Footer from '../Layout/Footer';

export default function FAQ() {
  const store = useSelector((state) => state);
  const trans = new Translator(store.language, 'faq');

  return (
    <div className={cl.containerMain}>
      <Head><title>Cero | FAQ</title></Head>
      <Header />
      <ContainerCustom>
        <PageInfo />
        <Grid container spacing={4} className={cl.container}>
          <Grid item sm={6}>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question1') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer1') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question2') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer2') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question3') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer3') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question4') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer4') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question5') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer5') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question6') }} />
              <div>
                {trans.t('answer6_1')}:
                <div>
                  <div><div /></div>
                  <span dangerouslySetInnerHTML={{ __html: trans.t('answer6_1_1') }} />
                </div>
                <div>
                  <div><div /></div>
                  <span dangerouslySetInnerHTML={{ __html: trans.t('answer6_1_2') }} />
                </div>
                <div>
                  <div><div /></div>
                  <span dangerouslySetInnerHTML={{ __html: trans.t('answer6_1_3') }} />
                </div>
              </div>
            </div>
            {/* <div> */}
            {/*  <p dangerouslySetInnerHTML={{ __html: trans.t('question7') }} /> */}
            {/*  <p dangerouslySetInnerHTML={{ __html: trans.t('answer7') }} /> */}
            {/* </div> */}
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question8') }} />
              <div>
                {trans.t('answer8_1')}:
                <div>
                  <div><div /></div>
                  <span dangerouslySetInnerHTML={{ __html: trans.t('answer8_1_1') }} />
                </div>
                <div>
                  <div><div /></div>
                  <span dangerouslySetInnerHTML={{ __html: trans.t('answer8_1_2') }} />
                </div>
              </div>
            </div>
          </Grid>
          <Grid item sm={6}>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question9') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer9') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question10') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer10') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question11') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer11') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question12') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer12') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question13') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer13') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question14') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer14') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question15') }} />
              <p dangerouslySetInnerHTML={{ __html: trans.t('answer15') }} />
            </div>
            <div>
              <p dangerouslySetInnerHTML={{ __html: trans.t('question16') }} />
              <div className={cl.contractAddress}>
                {trans.t('answer16_1')}:
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCTRopstenAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCBCRopstenAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCCCRopstenAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractLURopstenAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractFRopstenAddress}</span>
                </div>
                <br />
                {trans.t('answer16_2')}:
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCTRinkebyAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCBCRinkebyAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractCCCRinkebyAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractLURinkebyAddress}</span>
                </div>
                <div>
                  <div><div /></div>
                  <span>{process.env.contractFRinkebyAddress}</span>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </ContainerCustom>
      <Footer />
    </div>
  );
}
