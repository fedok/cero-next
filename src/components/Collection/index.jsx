import { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Head from 'next/head';
import CeroShortCard from 'elements/Cero/CeroShortCard';
import Switch1 from 'elements/Switch1';
import { useDispatch, useSelector } from 'react-redux';
import PageInfo from 'elements/PageInfo';
import Button1 from 'elements/Button1';
import { modalSetActiveWindow } from 'redux/reducers/src/modal/action';
import { findAccountTokens } from 'handlers/token.handler';
import Translator from 'handlers/translator';
import cl from './style.module.scss';
import Layout from '../Layout';
import { createBaseCeroProcess } from './handler';
import { MODAL_CREATE_FROM_SOUL } from 'components/modals/ModalLayout';

export default function Collection() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'collection');

  const [onlyActiveStatus, setOnlyActiveStatus] = useState(false);
  const [onlyActiveTokens, setOnlyActiveTokens] = useState([]);
  const [tokens, setTokens] = useState([]);

  useEffect(() => {
    setTokens(findAccountTokens(store));
  }, [store.contract.tokens]);

  const clickCreateNewCero = () => {
    dispatch(modalSetActiveWindow(MODAL_CREATE_FROM_SOUL));
  };

  const clickGetBaseCero = async () => {
    dispatch(createBaseCeroProcess());
  };

  const onChangeSwitch = (status) => {
    let newTokens = [];

    if (onlyActiveTokens.length === 0) {
      if (status === true) {
        for (let i = 0; i < tokens.length; i += 1) {
          if (tokens[i].isChild === true) {
            newTokens.push(tokens[i]);
          }
        }
      } else {
        newTokens = store.contract.tokens;
      }
    }

    setOnlyActiveStatus(status);
    setOnlyActiveTokens(newTokens);
  };

  const renderHeadContent = () => {
    if (tokens.length !== 0) {
      return (
        <section className={cl.filter}>
          <Grid container spacing={4}>
            <Grid item xs={12} md={6}>
              <Switch1
                onChange={(e) => onChangeSwitch(e)}
                name={trans.t('Hide collectible')}
                checked={onlyActiveStatus}
              />
            </Grid>
            <Grid container item xs={12} sm={12} md={6} justify="flex-end">
              {store.account.isContractOwner
                ? <Button1 name={trans.t('Get cero')} type="outlined-1" onClick={clickGetBaseCero} /> : ''}
              {store.account.souls > 0
                ? <Button1 name={trans.t('Create new +')} type="type-2" onClick={clickCreateNewCero} /> : '' }
            </Grid>
          </Grid>
        </section>
      );
    }

    if (store.contract.tokensLoadTime !== 0) {
      return (
        <section className={cl.filter}>
          <Grid container spacing={4}>
            <Grid item xs={12} md={8}>
              <p>{trans.t('Press the button and get your first cero')}!</p>
            </Grid>
            <Grid item xs={12} md={4}>
              <Button1 name={trans.t('Get first cero!')} type="type-2" onClick={clickGetBaseCero} />
            </Grid>
          </Grid>
        </section>
      );
    }

    return '';
  };

  const renderContent = () => {
    const tokensToRender = onlyActiveStatus === true ? onlyActiveTokens : tokens;

    if (tokensToRender.length !== 0) {
      return tokensToRender.map((tokenData, key) => (
        <Grid item xs={12} sm={6} md={4} lg={3} key={key}><CeroShortCard token={tokenData} /></Grid>
      ));
    }

    return '';
  };

  return (
    <Layout>
      <Head><title>Cero | Collection</title></Head>
      <PageInfo />

      {renderHeadContent()}

      <Grid container spacing={4}>
        {renderContent()}
      </Grid>
    </Layout>
  );
}
