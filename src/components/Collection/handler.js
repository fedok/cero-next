import { getAcceptedToCreateBaseToken, getTokensByAddress } from 'handlers/contracts/view.handler';
import { modalClose, modalSetActiveWindow } from 'redux/reducers/src/modal/action';
import { accountSoulsUpdate } from 'redux/reducers/src/account/action';
import { contractAddToken } from 'redux/reducers/src/contract/action';
import { handleMetamaskError } from 'handlers/error.handler';
import { createBaseCero } from 'handlers/contracts/send.handler';

export const createBaseCeroProcess = (isFromSoul = false) => async (dispatch, useState) => {
  const store = useState();
  const { account, gasPrice } = store.web3;

  dispatch(modalSetActiveWindow('modal-preloader'));

  try {
    await createBaseCero(account, gasPrice);
    const newTokens = await getTokensByAddress(account);

    // eslint-disable-next-line no-restricted-syntax
    for (const newToken of newTokens) {
      dispatch(contractAddToken(newToken));
    }

    if (isFromSoul) {
      const newSouls = await getAcceptedToCreateBaseToken(store.web3.account);
      dispatch(accountSoulsUpdate(newSouls));
    }

    dispatch(modalClose);
  } catch (e) {
    handleMetamaskError(dispatch, e.message);
  }
};
