import { modalClose, modalSetActiveWindow } from 'redux/reducers/src/modal/action';
import { levelUp } from 'handlers/contracts/send.handler';
import { handleMetamaskError } from 'handlers/error.handler';
import { findToken } from 'handlers/token.handler';
import { contractAddToken } from 'redux/reducers/src/contract/action';

function getNewCharacteristicPoints(points, oldCharPoints, type) {
  let newCharacteristic = oldCharPoints;

  if (type === 'plus') {
    if (points !== 0) {
      newCharacteristic = oldCharPoints + 1;
    }
  } else if (oldCharPoints - 1 < 0) {
    newCharacteristic = oldCharPoints;
  } else {
    newCharacteristic = oldCharPoints - 1;
  }

  return newCharacteristic;
}

function getNewPoints(oldPoints, oldCharPoints, newCharPoints) {
  let oldPointsLoc = oldPoints;

  if (oldCharPoints < newCharPoints) {
    oldPointsLoc -= 1;
  } else if (oldCharPoints > newCharPoints) {
    oldPointsLoc += 1;
  }

  return oldPointsLoc;
}

export function getNewDistribution(distribution, characteristic, type) {
  let newCharPoints = 0;
  let newPoints = 0;
  let newDistribution = distribution;

  // eslint-disable-next-line default-case
  switch (characteristic) {
    case 'strength':
      newCharPoints = getNewCharacteristicPoints(distribution.points, distribution.strength, type);
      newPoints = getNewPoints(distribution.points, distribution.strength, newCharPoints);
      // eslint-disable-next-line no-case-declarations
      const newHPPoints = process.env.baseHP + newCharPoints * process.env.baseHPIncrease;

      newDistribution = {
        ...distribution,
        strength: newCharPoints,
        points: newPoints,
        health: newHPPoints,
      };
      break;
    case 'agility':
      newCharPoints = getNewCharacteristicPoints(distribution.points, distribution.agility, type);
      newPoints = getNewPoints(distribution.points, distribution.agility, newCharPoints);
      newDistribution = {
        ...distribution,
        agility: newCharPoints,
        points: newPoints,
      };
      break;
    case 'protection':
      newCharPoints = getNewCharacteristicPoints(distribution.points, distribution.protection, type);
      newPoints = getNewPoints(distribution.points, distribution.protection, newCharPoints);
      newDistribution = {
        ...distribution,
        protection: newCharPoints,
        points: newPoints,
      };
      break;
    case 'magic':
      newCharPoints = getNewCharacteristicPoints(distribution.points, distribution.magic, type);
      newPoints = getNewPoints(distribution.points, distribution.magic, newCharPoints);
      newDistribution = {
        ...distribution,
        magic: newCharPoints,
        points: newPoints,
      };
      break;
  }

  return newDistribution;
}

export const levelUpProccess = (router, tokenNum, dist) => async (dispatch, useStore) => {
  const store = useStore();

  dispatch(modalSetActiveWindow('modal-preloader-level-up'));

  try {
    // eslint-disable-next-line max-len
    await levelUp(store.web3.account, tokenNum, dist.strength, dist.protection, dist.agility, dist.magic, store.web3.gasPrice);

    const newToken = await findToken(store, dispatch, tokenNum, false, true, store.web3.account);
    dispatch(contractAddToken(newToken));

    router.push('/collection');

    dispatch(modalClose);
  } catch (e) {
    handleMetamaskError(dispatch, e.message);
  }
};
