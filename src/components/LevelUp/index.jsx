/* eslint-disable max-len */

import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import PageInfo from 'elements/PageInfo';
import CeroFullCard from 'elements/Cero/CeroFullCard';
import { findToken, TOKEN_CLASSES } from 'handlers/token.handler';
import Button1 from 'elements/Button1';
import { showAlertWindow } from 'redux/reducers/src/modal/action';
import Translator from 'handlers/translator';
import Head from 'next/head';
import cl from './style.module.scss';
import Layout from '../Layout';
import { getNewDistribution, levelUpProccess } from './handler';
import Error404 from '../404';

export default function LevelUp() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();
  const trans = new Translator(store.language, 'levelUp');
  const transC = new Translator(store.language, 'common');

  const [token, setToken] = useState(undefined);
  const [distribution, setDistribution] = useState({
    health: process.env.baseHP,
    strength: 0,
    agility: 0,
    protection: 0,
    magic: 0,
    points: 0,
  });

  useEffect(async () => {
    if (store.contract.tokensLoadTime !== 0 && router.query.number !== undefined) {
      if (store.contract.pointsForDistribution !== 0 && distribution.points === 0) {
        setDistribution({ ...distribution, points: store.contract.pointsForDistribution });
      }

      const tokenLoc = await findToken(store, dispatch, router.query.number);
      if (tokenLoc.owner === store.web3.account && tokenLoc.isChild === true && tokenLoc.exp === tokenLoc.expToUp) {
        setToken(tokenLoc);
      } else {
        setToken(null);
      }
    }
  }, [store.contract.tokensLoadTime, store.contract.tokens, router.query.number]);

  const onClickBtn = (characteristic, type = 'minus') => {
    setDistribution(getNewDistribution(distribution, characteristic, type));
  };

  const onClickConfirmDistribution = () => {
    if (distribution.points !== 0) {
      const message = `${trans.t('It looks like you have not distributed all the characteristic points. You have')}
         &nbsp;<b><green>${distribution.points} ${trans.t('undistributed points')}</green></b>&nbsp;
         ${trans.t('left, please make your character stronger!')}`;

      const alertContent = { ...store.modal.alertContent, message };
      dispatch(showAlertWindow(alertContent));
    } else {
      dispatch(levelUpProccess(router, token.id, distribution));
    }
  };

  if (token === null) return <Error404 />;
  if (token === undefined) return <Layout><p>&nbsp;</p></Layout>;

  return (
    <Layout>
      <Head><title>Cero | Level up</title></Head>
      <PageInfo />
      <Grid container spacing={4}>
        <Grid item xs={12} className={cl.btnContainer}>
          <Button1 name={trans.t('Confirm distribution')} type="outlined-1" onClick={onClickConfirmDistribution} />
        </Grid>
        <Grid item xs={12} md={5} lg={3} className={cl.ceroCard}>
          <CeroFullCard token={token} renderRight={false} showLvlUp={false} />
        </Grid>
        <Grid item xs={12} md={7} lg={3} className={cl.gridFlex}>
          <div className={cl.level}>
            <div>
              <span>{transC.t('Level')}</span>
              <span>+1</span>
            </div>
            <p>
              {token.level} → <span>{token.level + 1}</span>
            </p>
            <p>{trans.t('The main indicator of the cero progress, the higher the level of the cero, the more valuable he is')}.</p>
          </div>
          <div className={cl.health}>
            <div>
              <span>{transC.t('Health')}</span>
              <span>+{distribution.health}</span>
            </div>
            <p>
              <span>{token.health + distribution.health}</span>
            </p>
            <p>{trans.t('The total health level of the character. Each level gives 50HP and 5HP for each protection point')}.</p>
          </div>
          <div className={cl.points}>
            <div>
              <span>{trans.t('Points for distribution')}</span>
            </div>
            <p>{distribution.points}</p>
            <p>{trans.t('The total number of available skill points that you can allocate to the characteristics of the cero')}.</p>
          </div>
        </Grid>
        <Grid item xs={12} md={6} lg={3} className={cl.gridFlex}>
          <div className={TOKEN_CLASSES.wizard === token.class ? `${cl.strength} ${cl.disabled}` : cl.strength}>
            <div>
              <span>{transC.t('')}Strength</span>
              <span>{distribution.strength > 0 ? `+${distribution.strength}` : ''}</span>
            </div>
            <p>
              {token.strength} → <span>{token.strength + distribution.strength}</span>
            </p>
            <p>
              {trans.t('Strength is the main attacking indicator for warrior and thief classes. The higher the strength, the more damage your cero deals per hit')}.
            </p>
            <div>
              <div onClick={() => onClickBtn('strength')}><span>-</span></div>
              <div onClick={() => onClickBtn('strength', 'plus')}><span>+</span></div>
            </div>
          </div>
          <div className={cl.agility}>
            <div>
              <span>{transC.t('Agility')}</span>
              <span>{distribution.agility > 0 ? `+${distribution.agility}` : ''}</span>
            </div>
            <p>
              {token.agility} → <span>{token.agility + distribution.agility}</span>
            </p>
            <p>
              {trans.t('Agility, one of the thief&apos;s and magic&apos;s primary indicators. Allows you to dodge an enemy attack with some chance without taking damage')}.
            </p>
            <div>
              <div onClick={() => onClickBtn('agility')}><span>-</span></div>
              <div onClick={() => onClickBtn('agility', 'plus')}><span>+</span></div>
            </div>
          </div>
        </Grid>
        <Grid item xs={12} md={6} lg={3} className={cl.gridFlex}>
          <div className={cl.protection}>
            <div>
              <span>{transC.t('Protection')}</span>
              <span>{distribution.protection > 0 ? `+${distribution.protection}` : ''}</span>
            </div>
            <p>
              {token.protection} → <span>{token.protection + distribution.protection}</span>
            </p>
            <p>
              {trans.t('Protection, one of the main indicators of a warrior. Allows you to block some of the damage dealt by your')}
              <br />
              {trans.t('opponent')}.
            </p>
            <div>
              <div onClick={() => onClickBtn('protection')}><span>-</span></div>
              <div onClick={() => onClickBtn('protection', 'plus')}><span>+</span></div>
            </div>
          </div>
          <div className={TOKEN_CLASSES.wizard === token.class ? cl.magic : `${cl.magic} ${cl.disabled}`}>
            <div>
              <span>{transC.t('Magic')}</span>
              <span>{distribution.magic > 0 ? `+${distribution.magic}` : ''}</span>
            </div>
            <p>
              {token.magic} → <span>{token.magic + distribution.magic}</span>
            </p>
            <p>
              {trans.t('Magic is the main attack indicator of the mage classes. Magic allows you to ignore some of the opponent&apos;s defence points')}.
            </p>
            <div>
              <div onClick={() => onClickBtn('magic')}><span>-</span></div>
              <div onClick={() => onClickBtn('magic', 'plus')}><span>+</span></div>
            </div>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}
