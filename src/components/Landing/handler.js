import { getExperienceCountForLevelUp } from 'handlers/math.handler';
import { getTokenDescription, getTokenHealth, TOKEN_CLASSES } from 'handlers/token.handler';

export function getToken1ForLanding() {
  const level = 6;
  const strength = 49;

  return {
    id: 538197,
    name: 'Disapproving directer',
    level,
    exp: 113,
    expToUp: getExperienceCountForLevelUp(level + 1),
    class: TOKEN_CLASSES.warrior,
    img: '/images/landing/ceroes/cero-7.svg',
    strength,
    protection: 25,
    agility: 30,
    magic: 0,
    health: getTokenHealth(level, strength),
    isChild: true,
    parent1: 1,
    parent2: 1,
    birthday: 1,
    description: getTokenDescription(TOKEN_CLASSES.warrior),
    owner: '',
  };
}

export function getToken2ForLanding() {
  const token = getToken1ForLanding();
  token.id = 558197;

  return token;
}

export async function requestMetamaskConnection() {
  if (!process.browser) return;

  const { ethereum } = window;
  if (ethereum) {
    await ethereum.send('eth_requestAccounts');
  }
}
