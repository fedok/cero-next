/* eslint-disable max-len */

import ContainerCustom from 'elements/Container';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import CeroFullCard from 'elements/Cero/CeroFullCard';
import CeroShortCard from 'elements/Cero/CeroShortCard';
import Translator from 'handlers/translator';
import Head from 'next/head';
import cl from './style.module.scss';
import Header from '../Layout/Header';
import Footer from '../Layout/Footer';
import { getToken1ForLanding, getToken2ForLanding, requestMetamaskConnection } from './handler';

export default function Landing() {
  const store = useSelector((state) => state);
  const trans = new Translator(store.language, 'landing');

  useEffect(async () => {
    if (store.web3.isConnected !== false) {
      await requestMetamaskConnection();
    }
  }, [store.web3.isConnected]);

  return (
    <div className={cl.container}>
      <Head><title>Cero | Home</title></Head>
      <Header />
      <ContainerCustom>
        <div>
          <Grid container spacing={4} className={cl.containerT1}>
            <img className={cl.background1} src="/images/landing/background-1.svg" alt="background-1_image" />
            <Grid item xs={12} md={7} lg={9}>
              <p>{trans.t('text1')}</p>
              <p>
                {trans.t('text2')}
              </p>
            </Grid>
            <Grid item xs={12} md={5} lg={3} className={cl.bookContainer}>
              <img src="/images/landing/book.svg" width="400" height="420" alt="book_image" />
            </Grid>
          </Grid>
          <Grid container spacing={4} className={cl.containerT2}>
            <Grid item xs={12}>
              <p className={cl.title}>
                {trans.t('text3')} <span className={cl.green}>{trans.t('nft1')}</span>{trans.t('nft2')}&nbsp;
                <span className={cl.green}>{trans.t('nft3')}</span>{trans.t('nft4')} <span className={cl.green}>{trans.t('nft5')}</span>{trans.t('nft6')}
              </p>
              <p dangerouslySetInnerHTML={{ __html: trans.t('text4') }} />
              <div>
                <img src="/images/landing/ceroes/cero-1.svg" alt="cero-1_image" />
                <img src="/images/landing/ceroes/cero-2.svg" alt="cero-1_image" />
                <img src="/images/landing/ceroes/cero-3.svg" alt="cero-1_image" />
                <img src="/images/landing/ceroes/cero-4.svg" alt="cero-1_image" />
                <img src="/images/landing/ceroes/cero-5.svg" alt="cero-1_image" />
                <img src="/images/landing/ceroes/cero-6.svg" alt="cero-1_image" />
              </div>
            </Grid>
          </Grid>
          <Grid container spacing={4} className={cl.containerT3}>
            <Grid item xs={12} md={6} lg={8}>
              <p className={cl.title}>{trans.t('text5')}!</p>
              <p>
                {trans.t('text6')}&nbsp;
                <span className={cl.red}>{trans.t('text7')}</span>&nbsp;
                {trans.t('text8')}&nbsp;
                <span className={cl.green}>{trans.t('text9')}.</span>
                <br /><br />
                {trans.t('text10')}&nbsp;
                <span className={cl.blue}>{trans.t('text11')}.</span>&nbsp;
                {trans.t('text12')}.&nbsp;
                <br /><br />
                {trans.t('text13')}
              </p>
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <img className={cl.background3} src="/images/landing/background-3.svg" alt="background-3_image" />
              <CeroShortCard token={getToken1ForLanding()} onClick={() => {}} />
            </Grid>
            {/*<Grid item xs={2} />*/}
          </Grid>
          <Grid container spacing={4} className={cl.containerT4}>
            <Grid item xs={12} sm={10} md={6} lg={5}>
              <img className={cl.background4} src="/images/landing/background-4.svg" alt="background-4_image" />
              <CeroFullCard token={getToken2ForLanding()} on />
            </Grid>
            <Grid item md={4} lg={5}>
              <p className={cl.title}>{trans.t('text14')}</p>
              <p>
                {trans.t('text15')}&nbsp;
                <span className={cl.red}>{trans.t('text16')}</span>&nbsp;
                {trans.t('text17')}
                <br /><br />

                {trans.t('text18')}&nbsp;
                <span className={cl.green}>{trans.t('text19')}</span>&nbsp;
                {trans.t('text20')}&nbsp;
                <span className={cl.blue}>{trans.t('text21')}</span>
                {trans.t('text22')}
                <br /><br />

                {trans.t('text23')}&nbsp;
              </p>
            </Grid>
          </Grid>
        </div>
      </ContainerCustom>
      <Footer />
    </div>
  );
}
