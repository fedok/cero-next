/* eslint-disable max-len */

import { useSelector } from 'react-redux';
import FireAnimation from 'elements/FireAnimation';
import Translator from 'handlers/translator';
import ModalLayout from '../ModalLayout';
import cl from './style.module.scss';

export default function ModalPreloaderType2() {
  const store = useSelector((state) => state);
  const trans = new Translator(store.language, 'modals');
  const { activeWindow } = store.modal;

  const renderImage = () => {
    switch (activeWindow) {
      case 'modal-preloader-fight':
        return <img src="/images/wood-mask/wood-mask-2.png" alt="wood-mask" />;
      case 'modal-preloader-level-up':
      case 'modal-preloader-create':
        return <img src="/images/wood-mask/wood-mask-4.png" alt="wood-mask" />;
      default:
        return '';
    }
  };

  const renderText = () => {
    switch (activeWindow) {
      case 'modal-preloader-fight':
        return (
          <p>{trans.t('The')} <span className={cl.red}>{trans.t('God of Battles')}</span> {trans.t('decides the fate of ceroes')}!<br />{trans.t('Please await the final decision')}...</p>
        );
      case 'modal-preloader-level-up':
        return (
          <p>{trans.t('The')} <span className={cl.green}>{trans.t('God of Creation')}</span> {trans.t('enhances your cero')}!<br />{trans.t('Please await')}...</p>
        );
      case 'modal-preloader-create':
        return (
          <p>{trans.t('The')} <span className={cl.green}>{trans.t('God of Creation')}</span> {trans.t('generate a new token')}!<br />{trans.t('Please await')}...</p>
        );
      default:
        return '';
    }
  };

  if (activeWindow !== 'modal-preloader-fight'
    && activeWindow !== 'modal-preloader-level-up'
    && activeWindow !== 'modal-preloader-create') return '';

  return (
    <ModalLayout modal={store.modal}>
      <div className={cl.container}>
        {renderImage()}
        <div>
          <FireAnimation />
          {renderText()}
          <FireAnimation />
        </div>
      </div>
    </ModalLayout>
  );
}
