import PropTypes from 'prop-types';
import cl from './style.module.scss';

export const MODAL_CHOICE_CERO = 'modal-choice-cero';
export const MODAL_WIN_LOSE = 'modal-win-loose';
export const MODAL_CREATE_FROM_SOUL = 'modal-create-from-soul';
export const MODAL_CREATION_RESULT = 'modal-creation-result';

export default function ModalLayout({ children, modal }) {
  const isAlignItemCenter = modal.activeWindow !== MODAL_CHOICE_CERO
    && modal.activeWindow !== MODAL_WIN_LOSE
    && modal.activeWindow !== MODAL_CREATION_RESULT;

  return (
    <div
      className={`
      ${cl.container} 
      ${cl[modal.isActive ? 'active' : 'inactive']}
      ${cl[isAlignItemCenter ? 'alignItemsCenter' : '']}
      `}
    >
      {children}
    </div>
  );
}

ModalLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  modal: PropTypes.object.isRequired,
};
