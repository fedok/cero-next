import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import CeroShortCard from 'elements/Cero/CeroShortCard';
import ContainerCustom from 'elements/Container';
import Button1 from 'elements/Button1';
import {
  modalClose,
  modalSetActiveWindow,
  modalSetContent,
  modalSetToken1,
  modalSetToken2
} from 'redux/reducers/src/modal/action';
import { findAccountTokens, findToken } from 'handlers/token.handler';
import { handleMetamaskError } from 'handlers/error.handler';
import { fight } from 'handlers/contracts/send.handler';
import cl from './style.module.scss';
import ModalLayout, { MODAL_CHOICE_CERO, MODAL_WIN_LOSE } from '../ModalLayout';
import { handleEvent } from './handler';

export default function ModalChoiceCero() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();

  const [activeTokenId, setActiveTokenId] = useState(null);
  const [tokens, setTokens] = useState([]);
  const [showArrows, setShowArrows] = useState(false);

  useEffect(() => {
    setTokens(findAccountTokens(store, true));

    if (window.screen.width < 1280 && tokens.length > 3) {
      setShowArrows(true);
    }
  }, [store.contract.tokens]);

  useEffect(() => {
    if (window.screen.width < 1200 && tokens.length > 2) {
      setShowArrows(true);
    } else if (window.screen.width < 1280 && tokens.length > 3) {
      setShowArrows(true);
    } else if (tokens.length > 4) {
      setShowArrows(true);
    }
  }, [tokens, store.modal]);

  const onClickClose = () => {
    dispatch(modalClose);
    dispatch(modalSetContent({ title: '', btn: '' }));
    setActiveTokenId(null);
  };

  const onClickBtn = async () => {
    setActiveTokenId(null);

    if (store.modal.nextActiveWindow === '') {
      dispatch(modalClose);
      return;
    }

    if (store.modal.nextActiveWindow === MODAL_WIN_LOSE) {
      dispatch(modalSetActiveWindow('modal-preloader-fight'));

      await handleEvent(dispatch);

      try {
        await fight(activeTokenId, store.web3.account, store.web3.gasPrice);
      } catch (e) {
        handleMetamaskError(dispatch, e.message);
      }

      return;
    }

    if (store.modal.nextActiveWindow === 'set-token-1' || store.modal.nextActiveWindow === 'set-token-2') {
      const activeToken = await findToken(store, dispatch, activeTokenId, false, false);

      if (store.modal.nextActiveWindow === 'set-token-1') {
        dispatch(modalSetToken1(activeToken));
      } else {
        dispatch(modalSetToken2(activeToken));
      }
      dispatch(modalClose);
      return;
    }

    console.log('Nex window not set or action for next window not found');
  };

  const renderContent = () => {
    if (tokens.length === 0) return '';

    return (
      <>
        <Grid container spacing={4} className={`${cl.container}`}>
          {tokens.map((token, key) => (
            <Grid item xs={12} md={5} lg={3} key={key}>
              <div className={`
                    ${cl.cardContainer} 
                    ${activeTokenId === Number(token.id) || activeTokenId === null ? '' : cl.inactive} 
                    ${activeTokenId === Number(token.id) && activeTokenId !== null ? cl.active : ''}
                  `}
              >
                <CeroShortCard
                  token={token}
                  onClick={() => setActiveTokenId(Number(token.id))}
                  showLvlUp={false}
                />
              </div>
            </Grid>
          ))}
        </Grid>

        <div className={cl.btn}>
          <Button1 name={store.modal.content.btn} inactive={activeTokenId === null} onClick={onClickBtn} />
        </div>
      </>
    );
  };

  if (store.modal.activeWindow !== MODAL_CHOICE_CERO) return '';
  return (
    <ModalLayout modal={store.modal}>
      <ContainerCustom>
        <div className={`${cl.title} ${!showArrows ? cl.fool : ''}`}>
          {tokens.length === 0 ? <p>Tokens not found</p> : <p>{store.modal.content.title}</p> }
          <div>
            {/* eslint-disable-next-line max-len */}
            <svg onClick={onClickClose} width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.2259 17.9974L35.1135 5.13804C35.6778 4.5736 35.9949 3.80804 35.9949 3.00979C35.9949 2.21155 35.6778 1.44599 35.1135 0.881549C34.5491 0.317103 33.7837 0 32.9855 0C32.1874 0 31.4219 0.317103 30.8576 0.881549L18 13.7709L5.14243 0.881549C4.57806 0.317103 3.81262 -5.9474e-09 3.01448 0C2.21635 5.94741e-09 1.4509 0.317103 0.886538 0.881549C0.322172 1.44599 0.00511486 2.21155 0.00511486 3.00979C0.00511485 3.80804 0.322172 4.5736 0.886538 5.13804L13.7741 17.9974L0.886538 30.8568C0.605624 31.1355 0.382658 31.467 0.230499 31.8323C0.0783397 32.1976 0 32.5894 0 32.9851C0 33.3808 0.0783397 33.7726 0.230499 34.1379C0.382658 34.5031 0.605624 34.8347 0.886538 35.1133C1.16516 35.3943 1.49664 35.6173 1.86187 35.7695C2.22709 35.9217 2.61883 36 3.01448 36C3.41014 36 3.80187 35.9217 4.1671 35.7695C4.53232 35.6173 4.86381 35.3943 5.14243 35.1133L18 22.224L30.8576 35.1133C31.1362 35.3943 31.4677 35.6173 31.8329 35.7695C32.1981 35.9217 32.5899 36 32.9855 36C33.3812 36 33.7729 35.9217 34.1381 35.7695C34.5034 35.6173 34.8348 35.3943 35.1135 35.1133C35.3944 34.8347 35.6173 34.5031 35.7695 34.1379C35.9217 33.7726 36 33.3808 36 32.9851C36 32.5894 35.9217 32.1976 35.7695 31.8323C35.6173 31.467 35.3944 31.1355 35.1135 30.8568L22.2259 17.9974Z" /></svg>
          </div>
        </div>

        {renderContent()}

      </ContainerCustom>
    </ModalLayout>
  );
}
