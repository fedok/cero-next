import {
  modalSetActiveWindow,
  modalSetIsAttackerWin, modalSetIsNewSoulPart,
  modalSetToken1,
  modalSetToken2,
} from 'redux/reducers/src/modal/action';
import { findFightLogs, findToken } from 'handlers/token.handler';
import { getAcceptedToCreateBaseToken, getTokenOwner } from 'handlers/contracts/view.handler';
import { accountSoulsUpdate } from 'redux/reducers/src/account/action';
import { FIGHT_CONTRACT, getContract } from 'handlers/web3.handler';
import { MODAL_WIN_LOSE } from 'components/modals/ModalLayout';

const afterCatchEvent = (attackerNum, defenderNum, isNewTokenAccepted, winner) => async (dispatch, useState) => {
  const store = useState();

  // Set attacker to store
  const attackerToken = await findToken(store, dispatch, attackerNum, false, true, store.web3.account);
  dispatch(modalSetToken1(attackerToken));

  // Set winner to store
  if (attackerNum === winner) {
    dispatch(modalSetIsAttackerWin(true));
    dispatch(modalSetIsNewSoulPart(isNewTokenAccepted));

    // Update souls
    if (isNewTokenAccepted) {
      const newSouls = await getAcceptedToCreateBaseToken(store.web3.account);
      dispatch(accountSoulsUpdate(newSouls));
    }
  } else {
    dispatch(modalSetIsAttackerWin(false));
  }

  // Set defender to store
  const defenderOwner = await getTokenOwner(defenderNum);
  const defenderToken = await findToken(store, dispatch, defenderNum, false, true, defenderOwner);
  dispatch(modalSetToken2(defenderToken));

  // Update fight logs
  await findFightLogs(store, dispatch, attackerNum, false, true);
  await findFightLogs(store, dispatch, defenderNum, false, true);

  // Render next modal window
  dispatch(modalSetActiveWindow(MODAL_WIN_LOSE));
};

export async function handleEvent(dispatch) {
  const contract = await getContract(FIGHT_CONTRACT);

  contract.events.FightResult({}, async (error, res) => {
    const { attacker, defender, isNewTokenAccepted, winner } = res.returnValues;
    dispatch(afterCatchEvent(attacker, defender, isNewTokenAccepted, winner));
  });
}
