import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import CeroShortCard from 'elements/Cero/CeroShortCard';
import ContainerCustom from 'elements/Container';
import Button1 from 'elements/Button1';
import { modalClose, modalSetContent, modalSetToken1, modalSetToken2 } from 'redux/reducers/src/modal/action';
import Translator from 'handlers/translator';
import { getLooseDescription, getWinDescription } from './handler';
import cl from './style.module.scss';
import ModalLayout, { MODAL_WIN_LOSE } from '../ModalLayout';

export default function ModalWinLose() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'modals');

  const { activeWindow, token1, token2, isAttackerWin, isNewSoulPart } = store.modal;

  const onClickTakeReward = () => {
    dispatch(modalClose);
    dispatch(modalSetContent({ title: '', btn: '' }));
    dispatch(modalSetToken1(null));
    dispatch(modalSetToken2(null));
  };

  const renderReward = () => {
    if (isAttackerWin === true) {
      return (
        <>
          <div>
            {/* eslint-disable-next-line max-len */}
            <img src="/images/svg/star.svg" alt="star_image" />
            <span>5 EXP</span>
          </div>
          {isNewSoulPart ? (
            <div>
              {/* eslint-disable-next-line max-len */}
              <img src="/images/svg/soul.svg" alt="soul_image" />
              <span>{trans.t('Part of soul')}</span>
            </div>
          ) : ''}
        </>
      );
    }

    return (
      <>
        <div>
          {/* eslint-disable-next-line max-len */}
          <img src="/images/svg/star.svg" alt="star_image" />
          <span>3 EXP</span>
        </div>
      </>
    );
  };

  if (activeWindow !== MODAL_WIN_LOSE || isAttackerWin === null) return '';
  return (
    <ModalLayout modal={store.modal}>
      <ContainerCustom>
        <div className={`${cl.container} ${isAttackerWin !== true ? cl.loose : ''}`}>
          <Grid container spacing={4} justify="center">
            <Grid item xs={12}>
              <p className={cl.title}>{trans.t('YOU')}&nbsp;
                <span>{isAttackerWin !== true ? trans.t('LOST') : trans.t('WIN')}</span></p>
            </Grid>
            <Grid item xs={12} sm={5} lg={3} className={cl.ceroContainerCustom}>
              {token1 !== null ? <CeroShortCard token={token1} onClick={() => {}} showLvlUp={false} /> : ''}
            </Grid>
            <p className={cl.vs}>VS</p>
            <Grid item xs={12} sm={5} lg={3} className={cl.ceroContainer2}>
              {token2 !== null ? <CeroShortCard token={token2} onClick={() => {}} showLvlUp={false} /> : ''}
            </Grid>
            <Grid item xs={12} lg={3}>
              <div className={cl.fightDescription}>
                <div>
                  <p>{trans.t('Your story')}</p>
                  <p>{isAttackerWin ? trans.t(getWinDescription()) : trans.t(getLooseDescription())}</p>
                  <p>{trans.t('Your reward')}</p>
                  {renderReward()}
                </div>
                <Button1 name={trans.t('Take reward!')} onClick={onClickTakeReward} />
              </div>
            </Grid>

          </Grid>
        </div>
      </ContainerCustom>
    </ModalLayout>
  );
}
