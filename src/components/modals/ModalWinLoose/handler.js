/* eslint-disable */
import { getRandomInt } from 'handlers/math.handler';

const descriptionsWin = ['winStory0', 'winStory1', 'winStory2', 'winStory3', 'winStory4'];

const descriptionLoose = ['lostStory0', 'lostStory1', 'lostStory2', 'lostStory3', 'lostStory4']

export function getWinDescription() {
  return descriptionsWin[getRandomInt(descriptionsWin.length - 1)];
}

export function getLooseDescription() {
  return descriptionLoose[getRandomInt(descriptionLoose.length - 1)];
}
