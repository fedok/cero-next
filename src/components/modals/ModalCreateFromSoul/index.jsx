import { useDispatch, useSelector } from 'react-redux';
import Button1 from 'elements/Button1';
import ContainerCustom from 'elements/Container';
import Grid from '@material-ui/core/Grid';
import { modalClose, modalSetContent } from 'redux/reducers/src/modal/action';
import Translator from 'handlers/translator';
import cl from './style.module.scss';
import ModalLayout, { MODAL_CREATE_FROM_SOUL } from '../ModalLayout';
import { createBaseCeroProcess } from '../../Collection/handler';

export default function ModalCreateFromSoul() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'modals');

  const onClickClose = () => {
    dispatch(modalClose);
    dispatch(modalSetContent({ title: '', btn: '' }));
  };

  const onClickCreate = () => {
    dispatch(createBaseCeroProcess(true));
  };

  if (store.modal.activeWindow !== MODAL_CREATE_FROM_SOUL) return '';
  return (
    <ModalLayout modal={store.modal}>
      <ContainerCustom>
        <Grid container spacing={4} justify="center" alignItems="center">
          <Grid item xs={12} sm={8} md={6} className={cl.container}>
            <img src="/images/svg/soul.svg" alt="404_image" />
            <p>{trans.t('Are you sure you want to turn one piece of soul into a new cero?')}</p>
            <div>
              <Button1 name={trans.t('No')} type="outlined-1" onClick={onClickClose} />
              <Button1 name={trans.t('Yes')} type="type-2" onClick={onClickCreate} />
            </div>
          </Grid>
        </Grid>
      </ContainerCustom>
    </ModalLayout>
  );
}
