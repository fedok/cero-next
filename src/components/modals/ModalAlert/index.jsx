import { useDispatch, useSelector } from 'react-redux';
import Button1 from 'elements/Button1';
import ContainerCustom from 'elements/Container';
import Grid from '@material-ui/core/Grid';
import { modalClose, modalSetContent } from 'redux/reducers/src/modal/action';
import Translator from 'handlers/translator';
import cl from './style.module.scss';
import ModalLayout from '../ModalLayout';

export default function ModalAlert() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'modals');
  const { alertContent } = store.modal;

  const onClickClose = () => {
    if (alertContent.btnCallback !== null) {
      alertContent.btnCallback();
    } else {
      dispatch(modalClose);
      dispatch(modalSetContent({ title: '', btn: '' }));
    }
  };

  if (store.modal.activeWindow !== 'modal-alert') return '';
  return (
    <ModalLayout modal={store.modal}>
      <ContainerCustom>
        <Grid container spacing={4} justify="center" alignItems="center">
          <Grid item xs={12} sm={8} md={6} className={cl.container}>
            <img src="/images/svg/alert-icon.svg" alt="404_image" />
            <p dangerouslySetInnerHTML={{ __html: alertContent.message }} />
            <Button1
              name={alertContent.btn !== '' ? trans.t(alertContent.btn) : trans.t('Back')}
              type="outlined-1"
              onClick={onClickClose}
            />
          </Grid>
        </Grid>
      </ContainerCustom>
    </ModalLayout>
  );
}
