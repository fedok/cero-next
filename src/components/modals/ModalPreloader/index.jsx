import { useSelector } from 'react-redux';
import FireAnimation from 'elements/FireAnimation';
import ModalLayout from '../ModalLayout';
import cl from './style.module.scss';

export default function ModalPreloader() {
  const store = useSelector((state) => state);

  if (store.modal.activeWindow !== 'modal-preloader') return '';
  return (
    <ModalLayout modal={store.modal}>
      <div className={cl.container}>
        <FireAnimation />
        <p>{store.modal.content.title}</p>
      </div>
    </ModalLayout>
  );
}
