import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Link from 'next/link';
import PageInfo from 'elements/PageInfo';
import CeroFullCard from 'elements/Cero/CeroFullCard';
import { findToken, tokenNumberToString } from 'handlers/token.handler';
import { fromSolDateToDateFormatType1 } from 'handlers/time.handler';
import { getTokenURI } from 'handlers/contracts/view.handler';
import Button1 from 'elements/Button1';
import { NETWORK_RINKEBY } from 'handlers/web3.handler';
import Translator from 'handlers/translator';
import Head from 'next/head';
import { getStats, setTokenUriProcess } from './handler';
import Error404 from '../404';
import cl from './style.module.scss';
import Layout from '../Layout';

export default function CeroInfoView() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();
  const trans = new Translator(store.language, 'tokenInfo');

  const [token, setToken] = useState(undefined);
  const [tokenURI, setTokenURI] = useState('');
  const [stats, setStats] = useState({
    wins: 0,
    loses: 0,
    winRate: 0,
    logs: [],
  });

  useEffect(async () => {
    if (store.contract.tokensLoadTime !== 0 && router.query.number !== undefined) {
      const tokenLoc = await findToken(store, dispatch, router.query.number, true, false, '', true);

      if (tokenLoc !== null) {
        const tokenURILoc = await getTokenURI(router.query.number);
        setTokenURI(tokenURILoc);

        const statsLoc = await getStats(store, dispatch, router.query.number);
        setStats(statsLoc);
      }

      setToken(tokenLoc);
    }
  }, [
    store.contract.tokensLoadTime,
    store.contract.tokens,
    store.contract.fightLogs,
    router.query.number,
    store.language,
  ]);

  const clickUpdateMetadata = async () => {
    dispatch(setTokenUriProcess(token.id));
  };

  const clickShowOnOpenSea = async () => {
    const link = `https://testnets.opensea.io/assets/${process.env.contractCTRinkebyAddress}/${token.id}`;
    window.open(link, '_blank');
  };

  const clickUpdateOnOpenSea = async () => {
    // eslint-disable-next-line max-len
    const link = `https://testnets-api.opensea.io/api/v1/asset/${process.env.contractCTRinkebyAddress}/${token.id}/?force_update=true`;
    window.open(link, '_blank');
  };

  if (token === null) return <Error404 />;
  if (token === undefined) return <Layout><p>&nbsp;</p></Layout>;

  return (
    <Layout>
      <Head><title>Cero | Token info</title></Head>
      <PageInfo headerDef={tokenNumberToString(token.id)} />
      <Grid container spacing={4}>
        {store.web3.network === NETWORK_RINKEBY ? (
          <Grid item xs={12} className={cl.topButtons}>
            {token.owner === store.web3.account && tokenURI.length === 0
              ? <Button1 name={trans.t('Set token metadata')} type="outlined-1" onClick={clickUpdateMetadata} /> : '' }
            {tokenURI.length > 0 ? (
              <>
                <Button1 name={trans.t('Update metadata')} type="outlined-1" onClick={clickUpdateOnOpenSea} />
                <Button1 name={trans.t('Open on OpenSea')} type="type-2" onClick={clickShowOnOpenSea} />
              </>
              ) : '' }
          </Grid>
        ) : ''}
        <Grid item xs={12} md={6} lg={5}><CeroFullCard token={token} /></Grid>
        <Grid item xs={12} md={6} lg={7}>
          <div className={cl.stats}>
            <div className={cl.head}>
              <div>
                <p>{trans.t('Wins')}</p>
                <p className={cl.color1}>{stats.wins}</p>
              </div>
              <div>
                <p>{trans.t('Defeats')}</p>
                <p className={cl.color2}>{stats.loses}</p>
              </div>
              <div>
                <p>{trans.t('Win rate')}</p>
                <p className={cl.color3}>{stats.winRate === undefined ? '-' : `${stats.winRate}%`}</p>
              </div>
            </div>
            <div className={cl.body}>
              <table>
                <thead>
                  { stats.logs.length > 0 ? (
                    <tr>
                      <th>{trans.t('Date')}</th>
                      <th>{trans.t('Opponent')}</th>
                      <th>{trans.t('Result')}</th>
                    </tr>
                ) : <tr /> }
                </thead>
                <tbody>
                  { stats.logs.map((log, key) => (
                    <tr key={key}>
                      <td>
                        <div />
                        <span>{log.date}</span>
                      </td>
                      <td>
                        <Link href={`/cero/${log.opponent}`}><a>{tokenNumberToString(log.opponent)}</a></Link>
                      </td>
                      <td className={log.isWin ? '' : cl.loose}>{log.isWin ? trans.t('Win') : trans.t('Loss')}</td>
                    </tr>
                )) }
                </tbody>
              </table>
            </div>
            {/* <div> */}
            {/*  <div className={cl.head}> */}
            {/*    <div> */}
            {/*      <p>{trans.t('Win rate')}</p> */}
            {/*      <p className={cl.color3}>{stats.winRate === undefined ? '-' : `${stats.winRate}%`}</p> */}
            {/*    </div> */}
            {/*  </div> */}
            {/*  <div className={cl.body}> */}
            {/*    <table> */}
            {/*      <thead> */}
            {/*        { stats.logs[1].length > 0 ? ( */}
            {/*          <tr> */}
            {/*            <th>{trans.t('Date')}</th> */}
            {/*            <th>{trans.t('Opponent')}</th> */}
            {/*            <th>{trans.t('Result')}</th> */}
            {/*          </tr> */}
            {/*      ) : <tr /> } */}
            {/*      </thead> */}
            {/*      <tbody> */}
            {/*        { stats.logs[1].map((log, key) => ( */}
            {/*          <tr key={key}> */}
            {/*            <td> */}
            {/*              <div /> */}
            {/*              <span>{log.date}</span> */}
            {/*            </td> */}
            {/*            <td> */}
            {/*              <Link href={`/cero/${log.opponent}`}><a>{tokenNumberToString(log.opponent)}</a></Link> */}
            {/*            </td> */}
            {/*            <td className={log.isWin ? '' : cl.loose}>{log.isWin ? trans.t('Win') : trans.t('Loss')}</td> */}
            {/*          </tr> */}
            {/*      )) } */}
            {/*      </tbody> */}
            {/*    </table> */}
            {/*  </div> */}
            {/* </div> */}
          </div>
          <div className={cl.additional}>
            <p>{trans.t('Owner')}</p>
            <p>{token.owner}</p>
          </div>
          <div className={cl.additional}>
            <p>{trans.t('Created')}</p>
            <p>{fromSolDateToDateFormatType1(token.birthday, store.language)}</p>
          </div>
          <div className={`${cl.additional} ${cl.parent}`}>
            <p>{trans.t('Parents')}</p>
            <p>
              <Link href={`/cero/${Number(token.parent1)}`}><a>{tokenNumberToString(token.parent1)}</a></Link>&nbsp;
              {trans.t('and')}&nbsp;
              <Link href={`/cero/${Number(token.parent2)}`}><a>{tokenNumberToString(token.parent2)}</a></Link>
            </p>
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}
