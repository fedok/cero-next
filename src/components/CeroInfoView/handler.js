import { fromSolDateToDateFormatType1 } from 'handlers/time.handler';
import { findFightLogs } from 'handlers/token.handler';
import { modalClose, modalSetActiveWindow } from '../../redux/reducers/src/modal/action';
import { setTokenURI } from '../../handlers/contracts/send.handler';
import { handleMetamaskError } from '../../handlers/error.handler';

export async function getStats(store, dispatch, tokenNum) {
  const num = String(tokenNum);
  const logs = await findFightLogs(store, dispatch, tokenNum, false);

  let wins = 0;
  let loses = 0;
  const newLogsAll = [];

  logs.forEach((log) => {
    const { attacker, defender, time, winner } = log;

    if (attacker === num && winner === num) {
      wins += 1;
      newLogsAll.push({ date: fromSolDateToDateFormatType1(time, store.language), opponent: defender, isWin: true });
    } else if (attacker === num && winner !== num) {
      loses += 1;
      newLogsAll.push({ date: fromSolDateToDateFormatType1(time, store.language), opponent: defender, isWin: false });
    } else if (defender === num && winner === num) {
      wins += 1;
      newLogsAll.push({ date: fromSolDateToDateFormatType1(time, store.language), opponent: attacker, isWin: true });
    } else if (defender === num && winner !== num) {
      loses += 1;
      newLogsAll.push({ date: fromSolDateToDateFormatType1(time, store.language), opponent: attacker, isWin: false });
    }
  });

  let winRate;
  if (wins === 0 && loses === 0) {
    winRate = undefined;
  } else {
    winRate = ((wins * 100) / (wins + loses)).toFixed(1);
  }

  return {
    wins,
    loses,
    winRate,
    logs: newLogsAll,
  };
}

export const setTokenUriProcess = (tokenNum) => async (dispatch, useState) => {
  const store = useState();
  const { account, gasPrice } = store.web3;

  dispatch(modalSetActiveWindow('modal-preloader'));

  try {
    let domain = window.location.hostname;
    domain = domain === 'localhost' || domain === '127.0.0.1' ? 'cero-next.vercel.app' : domain;

    const link = `https://${domain}/api/token-info/rinkeby/${tokenNum}`;

    await setTokenURI(tokenNum, link, account, gasPrice);

    dispatch(modalClose);
  } catch (e) {
    handleMetamaskError(dispatch, e.message);
  }
};
