import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import ContainerCustom from 'elements/Container';
import Button1 from 'elements/Button1';
import Translator from 'handlers/translator';
import { modalSetActiveWindow, modalSetNextActiveWindow, showChoiceTokenWindow } from 'redux/reducers/src/modal/action';
import { langChange } from 'redux/reducers/src/language/action';
import { pagesNav, pagesNavUnconnected } from 'components/Layout/Header/handler';
import { useEffect, useState } from 'react';
import cl from './style.module.scss';
import { MODAL_WIN_LOSE } from 'components/modals/ModalLayout';

export default function Header() {
  const router = useRouter();
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'header');
  const transM = new Translator(store.language, 'modals');

  const [pages, setPages] = useState(pagesNavUnconnected);

  useEffect(() => {
    if (store.web3.isConnected) {
      setPages(pagesNav);
    } else {
      setPages(pagesNavUnconnected);
    }
  }, [store.web3.isConnected]);

  const onClickLang = (payload) => {
    dispatch(langChange(payload));
  };

  const onClickFight = () => {
    dispatch(modalSetNextActiveWindow(MODAL_WIN_LOSE));
    dispatch(showChoiceTokenWindow({ title: transM.t('Who to send into battle?'), btn: transM.t('Fight!') }));
  };

  const onClickShowMenu = () => {
    dispatch(modalSetActiveWindow('modal-menu'));
  };

  return (
    <header className={cl.headerContainer}>
      <ContainerCustom>
        <div className={cl.internalContainer}>
          <Grid container spacing={4} justify="space-between">
            <Grid item xs={8} sm={6} md={3} lg={3} className={cl.leftBlock}>
              <span className={cl.logo} onClick={() => router.push('/')}>CERO</span>
              <div>
                <span
                  onClick={() => onClickLang('en')}
                  className={store.language === 'en' ? cl.active : ''}
                  role="button"
                >
                  En
                </span>
                <div />
                <span
                  onClick={() => onClickLang('ru')}
                  className={store.language === 'ru' ? cl.active : ''}
                  role="button"
                >
                  Ru
                </span>
              </div>
            </Grid>
            <Grid
              item
              xs={4}
              sm={6}
              lg={7}
              className={`${cl.rightBlock} ${store.web3.isConnected ? '' : cl.unconnected}`}
            >
              {pages.map((page, key) => (
                <Link href={page.link} key={key}>
                  <a className={router.pathname === page.link ? cl.active : ''}>{trans.t(page.alias)}</a>
                </Link>
              ))}

              {/* eslint-disable-next-line max-len */}
              <svg onClick={onClickShowMenu} width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.5 12.5C7.5 11.837 7.76339 11.2011 8.23223 10.7322C8.70107 10.2634 9.33696 10 10 10H40C40.663 10 41.2989 10.2634 41.7678 10.7322C42.2366 11.2011 42.5 11.837 42.5 12.5C42.5 13.163 42.2366 13.7989 41.7678 14.2678C41.2989 14.7366 40.663 15 40 15H10C9.33696 15 8.70107 14.7366 8.23223 14.2678C7.76339 13.7989 7.5 13.163 7.5 12.5Z" /><path d="M7.5 25C7.5 24.337 7.76339 23.7011 8.23223 23.2322C8.70107 22.7634 9.33696 22.5 10 22.5H40C40.663 22.5 41.2989 22.7634 41.7678 23.2322C42.2366 23.7011 42.5 24.337 42.5 25C42.5 25.663 42.2366 26.2989 41.7678 26.7678C41.2989 27.2366 40.663 27.5 40 27.5H10C9.33696 27.5 8.70107 27.2366 8.23223 26.7678C7.76339 26.2989 7.5 25.663 7.5 25Z" /><path d="M22.5 37.5C22.5 36.837 22.7634 36.2011 23.2322 35.7322C23.7011 35.2634 24.337 35 25 35H40C40.663 35 41.2989 35.2634 41.7678 35.7322C42.2366 36.2011 42.5 36.837 42.5 37.5C42.5 38.163 42.2366 38.7989 41.7678 39.2678C41.2989 39.7366 40.663 40 40 40H25C24.337 40 23.7011 39.7366 23.2322 39.2678C22.7634 38.7989 22.5 38.163 22.5 37.5Z" /></svg>
              { store.web3.isConnected
                ? <Button1 name={trans.t('To fight!')} onClick={() => onClickFight()} />
                : <Button1 name={trans.t('Get start!')} onClick={() => router.push('/collection')} /> }
            </Grid>
          </Grid>
        </div>
      </ContainerCustom>
    </header>
  );
}
