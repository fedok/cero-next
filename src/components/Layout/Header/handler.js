export const pagesNav = [
  { link: '/collection', alias: 'Collection' },
  { link: '/creation', alias: 'Creation' },
  { link: '/account', alias: 'Account' },
  { link: '/faq', alias: 'FAQ' },
  { link: '/rating', alias: 'Rating' },
];

export const pagesNavUnconnected = [
  { link: '/faq', alias: 'FAQ' },
];
