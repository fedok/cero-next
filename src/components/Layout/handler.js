import {
  getTokensByAddress,
  getPointForDistribution,
  getBaseHp,
  getBaseHpIncrease,
  getAcceptedToCreateBaseToken, checkIsAddressContractOwner,
} from 'handlers/contracts/view.handler';
import { accountSetIsContractOwner, accountSoulsUpdate } from 'redux/reducers/src/account/action';
import {
  contractAddTokens, contractSetBaseHP, contractSetBaseHPIncrease, contractSetFee,
  contractSetPointsForDistribution,
} from 'redux/reducers/src/contract/action';
import { web3SetIsContractInfoLoaded } from 'redux/reducers/src/web3/action';

export async function loadInitialInfoFromContract(dispatch, store) {
  // Upload account souls
  const newSouls = await getAcceptedToCreateBaseToken(store.web3.account);
  dispatch(accountSoulsUpdate(newSouls));

  // Upload contract owner
  const isContractOwner = await checkIsAddressContractOwner(store.web3.account);
  dispatch(accountSetIsContractOwner(isContractOwner));

  // Upload base HP
  const baseHp = await getBaseHp();
  dispatch(contractSetBaseHP(baseHp));

  // Upload base HP increase
  const baseHpIncrease = await getBaseHpIncrease();
  dispatch(contractSetBaseHPIncrease(baseHpIncrease));

  // Upload fee
  // const fee = await getFee();
  // dispatch(contractSetFee(fee));

  // Upload points for distribution
  const newPoints = await getPointForDistribution();
  dispatch(contractSetPointsForDistribution(newPoints));

  // Upload account tokens
  const newTokens = await getTokensByAddress(store.web3.account);
  dispatch(contractAddTokens(newTokens));

  dispatch(web3SetIsContractInfoLoaded(true));
}
