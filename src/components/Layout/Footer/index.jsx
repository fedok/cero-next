import ContainerCustom from 'elements/Container';
import { useRouter } from 'next/router';
import cl from './style.module.scss';

export default function Footer() {
  const router = useRouter();

  return (
    <footer className={cl.container}>
      <ContainerCustom>
        <div className={cl.innerContainer}>
          <div>
            <span onClick={() => router.push('/')}>CERO</span>
            <span>Ⓒ {(new Date()).getFullYear()} Cero. All Rights Reserved.</span>
          </div>
          <div>
            <a href="mailto: agoodminute@gmail.com" target="_blanc">
              {/* eslint-disable-next-line max-len */}
              <svg width="31" height="25" viewBox="0 0 31 25" xmlns="http://www.w3.org/2000/svg"><path d="M31 7.2V19.9219C31.0001 21.219 30.5078 22.4669 29.6241 23.4097C28.7404 24.3526 27.5323 24.9188 26.2477 24.9922L25.9625 25H5.0375C3.75079 25.0001 2.51282 24.5038 1.57754 23.613C0.642268 22.7222 0.0805979 21.5044 0.00775018 20.2094L0 19.9219V7.2L14.9606 15.1C15.127 15.1879 15.3121 15.2338 15.5 15.2338C15.6879 15.2338 15.873 15.1879 16.0394 15.1L31 7.2ZM5.0375 3.68829e-08H25.9625C27.2111 -0.000151585 28.4154 0.467181 29.3416 1.31136C30.2678 2.15553 30.8499 3.31638 30.9752 4.56875L15.5 12.7406L0.0248 4.56875C0.144961 3.36611 0.686727 2.24612 1.55271 1.4101C2.4187 0.574076 3.55211 0.0768547 4.7492 0.00781272L5.0375 3.68829e-08H25.9625H5.0375Z" /></svg>
            </a>
            {/* <a href="https://www.facebook.com/agooodminute/" target="_blanc"> */}
            {/*  /!* eslint-disable-next-line max-len *!/ */}
            {/* eslint-disable-next-line max-len */}
            {/*  <svg width="13" height="25" viewBox="0 0 13 25" xmlns="http://www.w3.org/2000/svg"><path d="M8.33333 14.375H11.3095L12.5 9.375H8.33333V6.875C8.33333 5.5875 8.33333 4.375 10.7143 4.375H12.5V0.175C12.1119 0.12125 10.6464 0 9.09881 0C5.86667 0 3.57143 2.07125 3.57143 5.875V9.375H0V14.375H3.57143V25H8.33333V14.375Z" /></svg> */}
            {/* </a> */}
            <a href="tg://resolve?domain=agoodminute" target="_blanc">
              {/* eslint-disable-next-line max-len */}
              <svg width="31" height="25" viewBox="0 0 31 25" xmlns="http://www.w3.org/2000/svg"><path d="M30.4119 2.27592L25.8851 23.4909C25.5435 24.9882 24.6529 25.3608 23.3873 24.6554L16.49 19.6046L13.1619 22.7855C12.7935 23.1515 12.4855 23.4576 11.7757 23.4576L12.2712 16.4769L25.0547 4.99767C25.6105 4.50522 24.9342 4.23238 24.1909 4.72483L8.3873 14.6136L1.58373 12.4974C0.103817 12.0383 0.0770314 11.0268 1.89176 10.3214L28.5034 0.133132C29.7355 -0.326037 30.8136 0.405972 30.4119 2.27592Z" /></svg>
            </a>
          </div>
        </div>
      </ContainerCustom>
    </footer>
  );
}
