import PropTypes from 'prop-types';
import ContainerCustom from 'elements/Container';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getWeb3Info } from 'handlers/web3.handler';
import { modalClose, modalSetActiveWindow, modalSetContent } from 'redux/reducers/src/modal/action';
import { useRouter } from 'next/router';
import Translator from 'handlers/translator';
import Footer from './Footer';
import Header from './Header';
import cl from './style.module.scss';
import { loadInitialInfoFromContract } from './handler';

export default function Layout({ children }) {
  const router = useRouter();
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'modals');

  useEffect(async () => {
    if (store.web3.isConnected === false) {
      dispatch(modalSetContent({ title: trans.t('Connection pending Metamask...'), btn: '' }));
      dispatch(modalSetActiveWindow('modal-preloader'));
      await getWeb3Info(dispatch);
    }
  }, []);

  useEffect(async () => {
    if (store.web3.isConnected === false) return;

    if (store.web3.isContractInfoLoaded === false) {
      await loadInitialInfoFromContract(dispatch, store);

      if (!router.pathname.includes('/rating')) {
        dispatch(modalClose);
        dispatch(modalSetContent({ title: '', btn: '' }));
      }
    }
  }, [store.web3.isConnected]);

  return (
    <div className={cl.mainContainer}>
      {store.web3.isConnected ? (
        <>
          <Header />
          <ContainerCustom>{children}</ContainerCustom>
          <Footer />
        </>
      ) : ''}
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
};
