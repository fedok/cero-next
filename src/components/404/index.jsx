import Grid from '@material-ui/core/Grid';
import ContainerCustom from 'elements/Container';
import Link from 'next/link';
import Head from 'next/head';
import cl from './style.module.scss';

export default function Error404() {
  return (
    <div className={cl.container}>
      <Head><title>Cero | 404</title></Head>
      <ContainerCustom>
        <Grid container spacing={4} direction="column" alignItems="center">
          <Grid item xs={2}>
            <img src="/images/svg/404.svg" alt="404_image" />
          </Grid>
          <Grid item xs={6}>
            <span>
              We are sorry, the page you requested can not be found. Please go back to the&nbsp;
              <Link href="/">homepage</Link> or contact us.
            </span>
          </Grid>
        </Grid>
      </ContainerCustom>
    </div>
  );
}
