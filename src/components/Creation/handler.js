import {
  modalSetActiveWindow, modalSetChildToken,
  modalSetToken1,
  modalSetToken2,
} from 'redux/reducers/src/modal/action';
import { findToken } from 'handlers/token.handler';
import { contractAddToken } from 'redux/reducers/src/contract/action';
import { CREATE_CHILD_CERO_CONTRACT, getContract } from 'handlers/web3.handler';
import { MODAL_CREATION_RESULT } from 'components/modals/ModalLayout';

const afterCatchEvent = (newChild, parent1, parent2) => async (dispatch, useState) => {
  const store = useState();

  // Skip tokens in modal window
  dispatch(modalSetToken1(null));
  dispatch(modalSetToken2(null));

  // Update parent1
  const parent1Old = await findToken(store, dispatch, parent1, false, false, store.web3.account);
  const parent1New = { ...parent1Old, isChild: false };
  dispatch(contractAddToken(parent1New));

  // Update parent1
  const parent2Old = await findToken(store, dispatch, parent2, false, false, store.web3.account);
  const parent2New = { ...parent2Old, isChild: false };
  dispatch(contractAddToken(parent2New));

  const child = await findToken(store, dispatch, newChild, false, true, store.web3.account);
  dispatch(modalSetChildToken(child));

  // Render next modal window
  dispatch(modalSetActiveWindow(MODAL_CREATION_RESULT));
};

export async function handleEventCreation(dispatch) {
  const contract = await getContract(CREATE_CHILD_CERO_CONTRACT);

  contract.events.ChildCreate({}, async (error, res) => {
    const { newChild, parent1, parent2 } = res.returnValues;
    dispatch(afterCatchEvent(newChild, parent1, parent2));
  });
}
