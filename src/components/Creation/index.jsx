/* eslint-disable max-len */

import { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import PageInfo from 'elements/PageInfo';
import Button1 from 'elements/Button1';
import CeroShortCard from 'elements/Cero/CeroShortCard';
import {
  modalSetActiveWindow,
  modalSetNextActiveWindow,
  showAlertWindow,
  showChoiceTokenWindow,
} from 'redux/reducers/src/modal/action';
import { createChildCero } from 'handlers/contracts/send.handler';
import { handleMetamaskError } from 'handlers/error.handler';
import Translator from 'handlers/translator';
import Head from 'next/head';
import Layout from '../Layout';
import cl from './style.module.scss';
import { handleEventCreation } from './handler';

export default function Creation() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'creation');

  const [isBtnInactive, setIsBtnInactive] = useState(true);
  const [ceroesToMerge, setCeroesToMerge] = useState({ left: null, right: null });

  useEffect(() => {
    setCeroesToMerge({ left: store.modal.token1, right: store.modal.token2 });
  }, [store.modal.token1, store.modal.token2]);

  useEffect(() => {
    if (ceroesToMerge.left !== null && ceroesToMerge.right !== null) {
      setIsBtnInactive(false);
    } else if (isBtnInactive === false) {
      setIsBtnInactive(true);
    }
  }, [ceroesToMerge]);

  const clickChooseCero = (side) => {
    if (side === 'left') {
      dispatch(modalSetNextActiveWindow('set-token-1'));
    } else {
      dispatch(modalSetNextActiveWindow('set-token-2'));
    }

    dispatch(showChoiceTokenWindow({ title: trans.t('Choice cero'), btn: trans.t('Choose!') }));
  };

  const clickConfirmCreation = async () => {
    if (ceroesToMerge.left.level !== ceroesToMerge.right.level) {
      const message = 'Looks like you picked ceroes of different levels! You need '
        + 'ceroes of the same level to create a new character.';

      const alertContent = { ...store.modal.alertContent, message };
      dispatch(showAlertWindow(alertContent));
    } else {
      dispatch(modalSetActiveWindow('modal-preloader-create'));

      await handleEventCreation(dispatch);
      try {
        await createChildCero(ceroesToMerge.left.id, ceroesToMerge.right.id, store.web3.account, store.web3.gasPrice);
      } catch (e) {
        handleMetamaskError(dispatch, e.message);
      }
    }
  };

  const renderSelection = (side) => {
    if (ceroesToMerge[side] !== null) {
      return (
        <CeroShortCard
          token={ceroesToMerge[side]}
          onClick={() => {}}
        />
      );
    }
    return (
      <div className={cl.blank}>
        <div />
        <div />
        <div />
        <div />
        <img src="/images/svg/hand.svg" alt="hand_image" />
      </div>
    );
  };

  return (
    <Layout>
      <Head><title>Cero | Creation</title></Head>
      <PageInfo />
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4} lg={3} onClick={() => clickChooseCero('left')}>
          {renderSelection('left')}
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} onClick={() => clickChooseCero('right')}>
          {renderSelection('right')}
        </Grid>
        <Grid item xs={12} md={4} lg={6} className={cl.description}>
          <div>
            <div>
              <div><div /></div>
              <p>
                {trans.t('It is necessary to choose two ceroes of the same level. Selected characters must not have')}
                &nbsp;<b className={cl.green}>&ldquo;{trans.t('collectible')}&rdquo;</b> {trans.t('status')}!
              </p>
            </div>
            <div>
              <div><div /></div>
              <p>{trans.t('The created cero will be one level higher than his parents')};</p>
            </div>
            <div>
              <div><div /></div>
              <p>
                {trans.t('The characteristics of the new cero will depend on his parents, and an additional 11 skill points will be allocated')}.
              </p>
            </div>
            <div>
              <div><div /></div>
              <p>
                {trans.t('After creating a new cero, the parent ceroes will receive the status of')}&nbsp;
                <b className={cl.green}>&ldquo;{trans.t('collectible')}&rdquo;</b>, {trans.t('they will no longer be available for upgrades or battles')},&nbsp;
                <b className={cl.red}>{trans.t('be careful')}!</b>
              </p>
            </div>
          </div>
          <Button1 name={trans.t('Confirm creation')} type="outlined-1" inactive={isBtnInactive} onClick={clickConfirmCreation} />
        </Grid>
      </Grid>
    </Layout>
  );
}
