import Grid from '@material-ui/core/Grid';
import PageInfo from 'elements/PageInfo';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { fromSolDateToDateFormatType1 } from 'handlers/time.handler';
import Button1 from 'elements/Button1';
import InputBase from 'elements/InputBase';
import { tokenNumberToString } from 'handlers/token.handler';
import { getTokensCount } from 'handlers/contracts/view.handler';
import { modalClose, modalSetActiveWindow } from 'redux/reducers/src/modal/action';
import Translator from 'handlers/translator';
import Head from 'next/head';
import { getStats, orderTokens, searchByKeyword, uploadTokens } from './handler';
import cl from './style.module.scss';
import Layout from '../Layout';

export default function Rating() {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const trans = new Translator(store.language, 'rating');
  const transT = new Translator(store.language, 'token');

  const [tokens, setTokens] = useState([]);
  const [stats, setStats] = useState({});
  const [order, setOrder] = useState({
    field: undefined,
    direction: undefined,
  });
  const [keyword, setKeyword] = useState('');

  useEffect(async () => {
    if (store.contract.tokensLoadTime !== 0) {
      const tokenCount = await getTokensCount();
      if (store.contract.tokens.length !== Number(tokenCount)) {
        dispatch(modalSetActiveWindow('modal-preloader'));
        dispatch(uploadTokens);
      } else {
        setTokens(store.contract.tokens);
        dispatch(modalClose);
      }
    }
  }, [store.contract.tokens]);

  useEffect(async () => {
    if (store.contract.tokensLoadTime !== 0) {
      setStats(getStats(store.contract.fightLogs));
    }
  }, [store.contract.fightLogs]);

  const onClickSearch = () => {
    if (keyword === '') {
      setTokens(store.contract.tokens);
      setOrder({ field: undefined, direction: undefined });
      return;
    }

    const newTokens = searchByKeyword(store.contract.tokens, keyword);
    setTokens(newTokens);
    setOrder({ field: undefined, direction: undefined });
  };

  const onClickSort = (e, field) => {
    let direction = 'ASC';
    if (order.field === field && order.direction === 'ASC') {
      direction = 'DESC';
    }

    const newOrder = { field, direction };

    setTokens(orderTokens(tokens, stats, newOrder));
    setOrder(newOrder);
  };

  const renderHeadCell = (field) => {
    const txt = field === 'winRate' ? 'Win rate' : field.charAt(0).toUpperCase() + field.slice(1);

    return (
      <th onClick={(e) => onClickSort(e, field)}>
        {/* eslint-disable-next-line max-len */}
        <svg className={order.field === field && order.direction === 'ASC' ? cl.active : ''} width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 18.3327V3.66602" stroke="#FDFDFD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" /><path d="M4.58398 11.916L11.0007 18.3327L17.4173 11.916" stroke="#FDFDFD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" /></svg>
        {/* eslint-disable-next-line max-len */}
        <svg className={order.field === field && order.direction === 'DESC' ? cl.active : ''} width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 18.3327V3.66602" stroke="#FDFDFD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" /><path d="M4.58398 11.916L11.0007 18.3327L17.4173 11.916" stroke="#FDFDFD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" /></svg>
        <span>{trans.t(txt)}</span>
      </th>
    );
  };

  return (
    <Layout>
      <Head><title>Cero | Rating</title></Head>
      <PageInfo />
      <Grid container spacing={4} className={cl.container}>
        <Grid item xs={12} sm={8} className={cl.searchInput}>
          <InputBase label={trans.t('Search by name or number...')} onChange={(e) => setKeyword(e.target.value)} />
        </Grid>
        <Grid item xs={12} sm={4} className={cl.searchBtn}>
          <Button1 name={trans.t('Search')} type="outlined-1" onClick={onClickSearch} />
        </Grid>
        <Grid item xs={12}>
          { tokens.length !== 0
          ? (
            <table>
              <thead>
                <tr>
                  <th>{trans.t('Image')}</th>
                  {renderHeadCell('name')}
                  {renderHeadCell('num')}
                  {renderHeadCell('lvl')}
                  {renderHeadCell('birthday')}
                  {renderHeadCell('str')}
                  {renderHeadCell('pr')}
                  {renderHeadCell('ag')}
                  {renderHeadCell('ma')}
                  {renderHeadCell('Wins')}
                  {renderHeadCell('Loss')}
                  {renderHeadCell('winRate')}
                </tr>
              </thead>
              <tbody>
                {tokens.map((token, key) => (
                  <tr key={key}>
                    <td>
                      <img src={token.img} alt={token.name} />
                    </td>
                    <td>
                      <Link href={`/cero/${Number(token.id)}`}>
                        <a>{`${transT.t(token.name.split(' ')[0])} ${transT.t(token.name.split(' ')[1])}` }</a>
                      </Link>
                    </td>
                    <td>
                      <Link href={`/cero/${Number(token.id)}`}><a>{tokenNumberToString(token.id)}</a></Link>
                    </td>
                    <td>{token.level}</td>
                    <td>{fromSolDateToDateFormatType1(token.birthday, store.language)}</td>
                    <td>{token.strength}</td>
                    <td>{token.protection}</td>
                    <td>{token.agility}</td>
                    <td>{token.magic}</td>
                    <td>{stats[token.id]?.wins}</td>
                    <td>{stats[token.id]?.loses}</td>
                    <td>
                      {stats[token.id] === undefined || stats[token.id].winRate === -1
                      ? '-' : `${stats[token.id].winRate.toFixed(3) }%`}
                    </td>
                  </tr>
              ))}
              </tbody>
            </table>
            )
          : (<p>{trans.t('Nothing found')}</p>) }
        </Grid>
      </Grid>
    </Layout>
  );
}
