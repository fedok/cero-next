import { tokenNumberToString } from 'handlers/token.handler';
import { getAllTokens, getFightLogs } from 'handlers/contracts/view.handler';
import { contractAddFightLogs, contractAddNewTokens } from 'redux/reducers/src/contract/action';

export function orderTokens(tokens, stats, order) {
  const sortedTokens = [...tokens];

  sortedTokens.map((token) => {
    if (stats[token.id] === undefined) return token;
    token.wins = stats[token.id].wins;
    token.loses = stats[token.id].loses;
    token.winRate = stats[token.id].winRate;
    return token;
  });

  let field = '';
  switch (order.field) {
    case 'num':
      field = 'id';
      break;
    case 'lvl':
      field = 'level';
      break;
    case 'str':
      field = 'strength';
      break;
    case 'pr':
      field = 'protection';
      break;
    case 'ag':
      field = 'agility';
      break;
    case 'ma':
      field = 'magic';
      break;
    case 'Wins':
      field = 'wins';
      break;
    case 'Loss':
      field = 'loses';
      break;
    default:
      field = order.field;
  }

  sortedTokens.sort((a, b) => {
    if (order.direction === 'ASC') {
      if (a[field] > b[field]) return 1;
      if (a[field] < b[field]) return -1;
    } else {
      if (a[field] < b[field]) return 1;
      if (a[field] > b[field]) return -1;
    }
    return 0;
  });

  return sortedTokens;
}

export function searchByKeyword(tokens, keyword) {
  const clearKeyword = keyword.trim();
  const newTokens = [];

  tokens.forEach((token) => {
    if (token.name.toLowerCase().includes(clearKeyword.toLowerCase()) === true) {
      newTokens.push(token);
      return;
    }

    if (tokenNumberToString(token.id).includes(clearKeyword) === true) {
      newTokens.push(token);
    }
  });

  return newTokens;
}

export const uploadTokens = async (dispatch, useState) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const store = useState();
  const tokensUploaded = await getAllTokens();

  const tokensExisted = store.contract.tokens;
  const tokensUploadedNotExisted = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const tokenUploaded of tokensUploaded) {
    let isTokenExistInStore = false;

    // eslint-disable-next-line no-restricted-syntax
    for (const tokenExisted of tokensExisted) {
      if (Number(tokenExisted.id) === Number(tokenUploaded.id)) {
        isTokenExistInStore = true;
        break;
      }
    }

    if (isTokenExistInStore) continue;
    tokensUploadedNotExisted.push(tokenUploaded);
  }

  const newTokens = [...tokensExisted, ...tokensUploadedNotExisted];
  const fightLogsOld = store.contract.fightLogs;

  const fightLogsNotExisted = {};

  // eslint-disable-next-line no-restricted-syntax
  for (const newToken of newTokens) {
    if (fightLogsOld[newToken.id] !== undefined) continue;

    const logs = await getFightLogs(newToken.id);
    if (logs !== null) {
      fightLogsNotExisted[newToken.id] = logs;
    }
  }

  if (Object.keys(fightLogsNotExisted).length > 0) {
    // console.log('Logs uploaded: ', fightLogsNotExisted);
    dispatch(contractAddFightLogs(fightLogsNotExisted));
  }

  if (tokensUploadedNotExisted.length > 0) {
    // console.log('Tokens uploaded', tokensUploadedNotExisted);
    dispatch(contractAddNewTokens(tokensUploadedNotExisted));
  }
};

export function getStats(tokensLogs) {
  const stats = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const [tokenNum, tokensLog] of Object.entries(tokensLogs)) {
    let wins = 0;
    let loses = 0;

    tokensLog.forEach((log) => {
      const { attacker, defender, winner } = log;

      if (attacker === tokenNum && winner === tokenNum) wins += 1;
      else if (attacker === tokenNum && winner !== tokenNum) loses += 1;
      else if (defender === tokenNum && winner === tokenNum) wins += 1;
      else if (defender === tokenNum && winner !== tokenNum) loses += 1;
    });

    let winRate;
    if (wins === 0 && loses === 0) {
      winRate = -1;
    } else {
      winRate = ((wins * 100) / (wins + loses));
    }

    stats[tokenNum] = { wins, loses, winRate };
  }

  return stats;
}
