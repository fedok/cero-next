import { getCero } from 'handlers/contracts/view.handler';
import Web3 from 'web3';
import { getContractABI, getContractAddress, CERO_TOKEN_CONTRACT } from 'handlers/web3.handler';

async function handler(req, res) {
  const { number } = req.query;

  const web3 = new Web3(new Web3.providers.HttpProvider(
    'https://rinkeby.infura.io/v3/875e92049d46477ba5fd3a0d22f7b7c3',
  ));

  const contractABI = getContractABI(CERO_TOKEN_CONTRACT);
  const contractAddress = getContractAddress('0x4', CERO_TOKEN_CONTRACT);
  const contract = new web3.eth.Contract(contractABI, contractAddress);

  const token = await getCero(number, '', contract);

  const totalPoint = token.strength + token.protection + token.agility + token.magic;

  res.json({
    name: token.name,
    description: token.description,
    image_url: `https://${req.headers.host}${token.img}`,
    attributes: [
      {
        display_type: 'date',
        trait_type: 'birthday',
        value: token.birthday,
      },
      {
        trait_type: 'Level',
        value: token.level,
      },
      {
        trait_type: 'Class',
        value: token.class,
      },
      {
        trait_type: 'Experience',
        value: token.exp,
      },
      {
        trait_type: 'First parent',
        value: token.parent1,
      },
      {
        trait_type: 'Second parent',
        value: token.parent2,
      },
      {
        trait_type: 'Health',
        value: token.health,
      },
      {
        trait_type: 'Strength',
        value: token.strength,
        max_value: totalPoint,
      },
      {
        trait_type: 'Protection',
        value: token.protection,
        max_value: totalPoint,
      },
      {
        trait_type: 'Agility',
        value: token.agility,
        max_value: totalPoint,
      },
      {
        trait_type: 'Magic',
        value: token.magic,
        max_value: totalPoint,
      },
    ],
  });
}

export default handler;
