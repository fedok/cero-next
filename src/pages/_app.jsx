import PropTypes from 'prop-types';
import { wrapper } from 'redux/store';
import 'styles/globals.scss';
import ModalChoiceCero from 'components/modals/ModalChoiceCero';
import ModalWinLose from 'components/modals/ModalWinLoose';
import ModalPreloader from 'components/modals/ModalPreloader';
import ModalPreloaderType2 from 'components/modals/ModalPreloaderType2';
import ModalAlert from 'components/modals/ModalAlert';
import ModalCreateFromSoul from 'components/modals/ModalCreateFromSoul';
import ModalCreationResult from 'components/modals/ModalCreationResult';
import ModalMenu from 'components/modals/ModalMenu';

const MyApp = ({ Component, pageProps }) => (
  <>
    <ModalPreloader />
    <ModalPreloaderType2 />
    <ModalChoiceCero />
    <ModalWinLose />
    <ModalAlert />
    <ModalCreateFromSoul />
    <ModalCreationResult />
    <ModalMenu />
    <Component {...pageProps} />
  </>
);

MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.object,
};

MyApp.defaultProps = {
  pageProps: {},
};

export default wrapper.withRedux(MyApp);
