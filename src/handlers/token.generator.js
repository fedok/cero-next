import { getExperienceCountForLevelUp, getRandomInt } from 'handlers/math.handler';
import { getTokenDescription, getTokenHealth, getTokenImageByClassAndNum, TOKEN_CLASSES } from './token.handler';

const names1 = [
  'Bomb',
  'Chronic',
  'Epic',
  'Catastrophic',
  'Amusing',
  'Madden',
  'Melancholic',
  'Tattered',
  'Crying',
  'Compulsive',
  'Thirsty',
  'Exhausted',
  'Throbbing',
  'Obstinate',
  'Disapproving',
];

const names2 = [
  'devil',
  'hairline',
  'kid',
  'daddy',
  'director',
  'amanita',
  'hump',
  'mandarin',
  'horse',
  'smoke',
  'bike',
  'sock',
  'nose',
  'rooster',
  'drink',
];

const classes = [
  TOKEN_CLASSES.warrior,
  TOKEN_CLASSES.thief,
  TOKEN_CLASSES.wizard,
];

function generateBaseToken() {
  const id = getRandomInt(1000000);
  const name = `${names1[getRandomInt(names1.length - 1)]} ${names2[getRandomInt(names1.length - 1)]}`;
  const level = getRandomInt(12, 1);
  const expToUp = getExperienceCountForLevelUp(level);
  const exp = getRandomInt(expToUp, getExperienceCountForLevelUp(level - 1));
  const tokenClass = classes[getRandomInt(classes.length - 1)];
  const img = getTokenImageByClassAndNum(id, tokenClass);
  const strength = tokenClass !== TOKEN_CLASSES.wizard ? getRandomInt(999) : 0;
  const protection = getRandomInt(999);
  const agility = getRandomInt(999);
  const magic = tokenClass === TOKEN_CLASSES.wizard ? getRandomInt(999) : 0;
  const health = getTokenHealth(level, strength);
  const birthday = getRandomInt(1620991900);
  const description = getTokenDescription(tokenClass);

  return { id,
    name,
    level,
    exp,
    expToUp,
    class: tokenClass,
    img,
    strength,
    protection,
    agility,
    magic,
    health,
    isChild: true,
    parent1: 0,
    parent2: 0,
    birthday,
    description };
}

export function generateTestTokens(count = 28) {
  return new Promise((resolve) => setTimeout(() => {
    const tokens = [];
    for (let i = 0; i < count; i += 1) {
      const token = generateBaseToken();
      if (i === 0) {
        token.id = 1;
      }
      tokens.push(token);
    }

    for (let i = 0; i < 2; i += 1) {
      const parent1Index = getRandomInt(tokens.length - 1);
      const parent2Index = getRandomInt(tokens.length - 1);
      const childIndex = getRandomInt(tokens.length - 1);

      tokens[parent1Index].isChild = false;
      tokens[parent2Index].isChild = false;
      tokens[childIndex].parent1 = parent1Index;
      tokens[childIndex].parent2 = parent2Index;
    }

    for (let i = 0; i < 4; i += 1) {
      const index = getRandomInt(tokens.length - 1);
      tokens[index].exp = tokens[index].expToUp;
    }

    resolve(tokens);
  }, 2500));
}
