// eslint-disable-next-line import/no-cycle
import { transferSolTokensToProjectTokens, transferSolTokenToProjectToken } from '../token.handler';
import { CERO_TOKEN_CONTRACT, FIGHT_CONTRACT, getContract, LEVEL_UP_CONTRACT } from '../web3.handler';

export async function getAllTokens() {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const tokensInfo = await contract.methods.getAllTokens().call();
  const tokens = transferSolTokensToProjectTokens([tokensInfo]);
  return tokens;
}

export async function getTokensByAddress(tokenOwner) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const tokensInfo = await contract.methods.getTokensByAddress(tokenOwner).call();
  const tokens = transferSolTokensToProjectTokens(tokensInfo, tokenOwner);
  return tokens;
}

export async function getTokensCount() {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const tokensCount = await contract.methods.getTokensCount().call();
  return tokensCount;
}

export async function getAcceptedToCreateBaseToken(tokenOwner) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const tokensNum = await contract.methods.acceptedToCreateBaseToken(tokenOwner).call();
  return tokensNum;
}

export async function getCero(tokenNum, tokenOwner = '', contract = null) {
  const contractLoc = contract ?? await getContract(CERO_TOKEN_CONTRACT);

  try {
    const token = await contractLoc.methods.ceroes(String(tokenNum)).call();
    return transferSolTokenToProjectToken(token, tokenNum, tokenOwner);
  } catch (e) {
    console.log('Token with this number not found!');
    return null;
  }
}

export async function getPointForDistribution() {
  const contract = await getContract(LEVEL_UP_CONTRACT);

  const res = await contract.methods.pointForDistribution().call();
  return Number(res);
}

// export async function balanceOf(tokenOwner) {
//   const contract = await getContract(CERO_TOKEN_CONTRACT);
//
//   const tokens = await contract.methods.balanceOf(tokenOwner).call();
// }

export async function checkIsAddressContractOwner(address) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const isOwner = await contract.methods.isOwner(address).call();
  return isOwner;
}

export async function getTokenOwner(tokenNum) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const tokenOwner = await contract.methods.ownerOf(tokenNum).call();
  return tokenOwner;
}

export async function getBaseHp() {
  const contract = await getContract(FIGHT_CONTRACT);

  const res = await contract.methods.baseHP().call();
  return Number(res);
}

export async function getBaseHpIncrease() {
  const contract = await getContract(FIGHT_CONTRACT);

  const res = await contract.methods.baseHPIncrease().call();
  return Number(res);
}

export async function getTokenURI(tokenNum) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const res = await contract.methods.tokenURI(tokenNum).call();
  return res;
}

export async function getFee() {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  const res = await contract.methods.fee().call();
  return res;
}

export async function getFightLogs(tokenNum) {
  const contract = await getContract(FIGHT_CONTRACT);

  try {
    const res = await contract.methods.getLogsForToken(tokenNum).call();
    return res;
  } catch (e) {
    console.log('Token with this number not found!');
    return null;
  }
}
