import {
  CERO_TOKEN_CONTRACT,
  CREATE_BASE_CERO_CONTRACT,
  CREATE_CHILD_CERO_CONTRACT,
  FIGHT_CONTRACT,
  getContract,
  LEVEL_UP_CONTRACT,
} from '../web3.handler';

export async function fight(tokenNum, tokenOwner, gasPrice) {
  const contract = await getContract(FIGHT_CONTRACT);

  await contract.methods.fight(tokenNum).send({ from: tokenOwner, gasPrice });
}

export async function createChildCero(tokenNum1, tokenNum2, tokenOwner, gasPrice) {
  const contract = await getContract(CREATE_CHILD_CERO_CONTRACT);

  await contract.methods.createChildCero(tokenNum1, tokenNum2).send({ from: tokenOwner, gasPrice });
}

export async function createBaseCero(tokenOwner, gasPrice) {
  const contract = await getContract(CREATE_BASE_CERO_CONTRACT);

  await contract.methods.createBaseCero(tokenOwner).send({ from: tokenOwner, gasPrice });
}

export async function levelUp(tokenOwner, tokenNum, st, pr, ag, ma, gasPrice) {
  const contract = await getContract(LEVEL_UP_CONTRACT);

  await contract.methods.levelUp(String(tokenNum), String(st), String(pr), String(ag), String(ma))
    .send({ from: tokenOwner, gasPrice });
}

export async function setTokenURI(tokenNum, tokenURI, tokenOwner, gasPrice) {
  const contract = await getContract(CERO_TOKEN_CONTRACT);

  await contract.methods.setTokenURI(tokenNum, tokenURI).send({ from: tokenOwner, gasPrice });
}
