import { modalSetContent, showAlertWindow } from 'redux/reducers/src/modal/action';

function replaceAll(string, search, replace) {
  return string.split(search).join(replace);
}

export function handleMetamaskError(dispatch, message) {
  const regexp1 = new RegExp("revert [a-zA-Z0-9':\\]\\[ -]*.");
  const result1 = regexp1.exec(message);

  if (result1 !== null) {
    const regexp11 = new RegExp('- [a-zA-z\' ]*.');
    const result11 = regexp11.exec(result1[0]);

    if (result11 === null) {
      dispatch(showAlertWindow({ message: result1[0], btn: '', btnCallback: null }));
      return;
    }

    const newMessage = result11[0].replace('- ', '');
    dispatch(showAlertWindow({ message: newMessage, btn: '', btnCallback: null }));
    return;
  }

  const regexp2 = new RegExp('"message":"[a-zA-Z0-9 \'.:]*"');
  const result2 = regexp2.exec(message);

  if (result2 !== null) {
    let newMessage = result2[0].replace('"message":', '');
    newMessage = replaceAll(newMessage, '"', '');
    newMessage = newMessage.charAt(0).toUpperCase() + newMessage.slice(1);
    dispatch(showAlertWindow({ message: newMessage, btn: '', btnCallback: null }));
    return;
  }

  dispatch(modalSetContent({ title: '', btn: '' }));
  dispatch(showAlertWindow({ message, btn: '', btnCallback: null }));
}
