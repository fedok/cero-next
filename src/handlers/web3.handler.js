import Web3 from 'web3';
import { showAlertWindow } from 'redux/reducers/src/modal/action';
import {
  web3AccountChange,
  web3BalanceChange, web3GasPriceChanged,
  web3NetworkChange,
  web3SetIsConnected,
} from 'redux/reducers/src/web3/action';

export const CERO_TOKEN_CONTRACT = 'CeroToken';
export const CREATE_BASE_CERO_CONTRACT = 'CreateBaseCero';
export const CREATE_CHILD_CERO_CONTRACT = 'CreateChildCero';
export const LEVEL_UP_CONTRACT = 'LevelUp';
export const FIGHT_CONTRACT = 'Fight';

export const NETWORK_MAIN = 'Ethereum Main Network';
export const NETWORK_ROPSTEN = 'Ropsten Test Network';
export const NETWORK_RINKEBY = 'Rinkeby Test Network';
export const NETWORK_GOERLI = 'Goerli Test Network';
export const NETWORK_KOVAN = 'Kovan Test Network';
export const NETWORK_VELAS_TESTNET = 'Velas Test Network';
export const NETWORK_LOCALHOST = 'localhost';

export function getWeb3Obj() {
  if (!process.browser) return undefined;

  const { ethereum } = window;

  return new Web3(ethereum);
}

function getChainName(chainId) {
  let chainName = '';
  switch (chainId) {
    case '0x1':
      chainName = NETWORK_MAIN;
      break;
    case '0x3':
      chainName = NETWORK_ROPSTEN;
      break;
    case '0x4':
      chainName = NETWORK_RINKEBY;
      break;
    case '0x5':
      chainName = NETWORK_GOERLI;
      break;
    case '0x2a':
      chainName = NETWORK_KOVAN;
      break;
    case '0x6f':
      chainName = NETWORK_VELAS_TESTNET;
      break;

    default:
      chainName = NETWORK_LOCALHOST;
  }

  return chainName;
}

export async function getWeb3Info(dispatch) {
  if (!process.browser) return;

  const { ethereum } = window;

  if (ethereum) {
    try {
      // Generate events
      ethereum.on('chainChanged', () => {
        window.location.reload();
      });

      ethereum.on('accountsChanged', () => {
        window.location.reload();
      });

      await ethereum.send('eth_requestAccounts');

      // Get chain id and name
      const chainId = await ethereum.request({ method: 'eth_chainId' });
      const chainName = getChainName(chainId);
      // eslint-disable-next-line max-len
      if (chainName !== NETWORK_LOCALHOST && chainName !== NETWORK_ROPSTEN && chainName !== NETWORK_RINKEBY && chainName !== NETWORK_VELAS_TESTNET) {
        // eslint-disable-next-line max-len
        throw new Error('Sorry, for now we support only Ropsten, Rinkeby and Velas Testnet network. Please, change your network in Metamask.');
      }

      // Get accounts, return empty array if not connected to network
      const accounts = await ethereum.request({ method: 'eth_accounts' });
      if (accounts.length === 0) {
        // eslint-disable-next-line max-len
        throw new Error('For the site to work correctly, you must be connected to the network via metamask. Please use metamask, log in, accept connection to this site and reload the page.');
      }

      // Get web3 object
      const web3 = getWeb3Obj();

      await web3.eth.getGasPrice().then((gasPrice) => {
        if (chainId === '0x6f') {
          gasPrice = 1;
        }

        dispatch(web3GasPriceChanged(gasPrice));
      });

      // Get account balance (need valid account)
      await web3.eth.getBalance(accounts[0], (err, balance) => {
        dispatch(web3BalanceChange(web3.utils.fromWei(balance)));
      });

      // Set data to storage
      dispatch(web3NetworkChange(chainName));
      dispatch(web3AccountChange(accounts[0].toLowerCase()));
      dispatch(web3SetIsConnected(true));
    } catch (e) {
      dispatch(showAlertWindow({ message: e.message, btn: 'Reload', btnCallback: () => window.location.reload() }));
      console.log(e.message);
    }
  }
}

export function getContractAddress(chainId, contractName) {
  if (chainId === '0x3') {
    switch (contractName) {
      case CERO_TOKEN_CONTRACT:
        return process.env.contractCTRopstenAddress;
      case CREATE_BASE_CERO_CONTRACT:
        return process.env.contractCBCRopstenAddress;
      case CREATE_CHILD_CERO_CONTRACT:
        return process.env.contractCCCRopstenAddress;
      case LEVEL_UP_CONTRACT:
        return process.env.contractLURopstenAddress;
      case FIGHT_CONTRACT:
        return process.env.contractFRopstenAddress;
      default:
        return '';
    }
  }

  if (chainId === '0x4') {
    switch (contractName) {
      case CERO_TOKEN_CONTRACT:
        return process.env.contractCTRinkebyAddress;
      case CREATE_BASE_CERO_CONTRACT:
        return process.env.contractCBCRinkebyAddress;
      case CREATE_CHILD_CERO_CONTRACT:
        return process.env.contractCCCRinkebyAddress;
      case LEVEL_UP_CONTRACT:
        return process.env.contractLURinkebyAddress;
      case FIGHT_CONTRACT:
        return process.env.contractFRinkebyAddress;
      default:
        return '';
    }
  }

  if (chainId === '0x6f') {
    switch (contractName) {
      case CERO_TOKEN_CONTRACT:
        return process.env.contractCTVelasTestnetAddress;
      case CREATE_BASE_CERO_CONTRACT:
        return process.env.contractCBCVelasTestnetAddress;
      case CREATE_CHILD_CERO_CONTRACT:
        return process.env.contractCCCVelasTestnetAddress;
      case LEVEL_UP_CONTRACT:
        return process.env.contractLUVelasTestnetAddress;
      case FIGHT_CONTRACT:
        return process.env.contractFVelasTestnetAddress;
      default:
        return '';
    }
  }

  switch (contractName) {
    case CERO_TOKEN_CONTRACT:
      return process.env.contractCTAddress;
    case CREATE_BASE_CERO_CONTRACT:
      return process.env.contractCBCAddress;
    case CREATE_CHILD_CERO_CONTRACT:
      return process.env.contractCCCAddress;
    case LEVEL_UP_CONTRACT:
      return process.env.contractLUAddress;
    case FIGHT_CONTRACT:
      return process.env.contractFAddress;
    default:
      return '';
  }
}

export function getContractABI(contractName) {
  switch (contractName) {
    case CERO_TOKEN_CONTRACT:
      return process.env.contractCTABI;
    case CREATE_BASE_CERO_CONTRACT:
      return process.env.contractCBCABI;
    case CREATE_CHILD_CERO_CONTRACT:
      return process.env.contractCCCABI;
    case LEVEL_UP_CONTRACT:
      return process.env.contractLUABI;
    case FIGHT_CONTRACT:
      return process.env.contractFABI;
    default:
      return '';
  }
}

export async function getContract(contractName) {
  if (!process.browser) return undefined;

  const { ethereum } = window;
  const chainId = await ethereum.request({ method: 'eth_chainId' });

  const contractAddress = getContractAddress(chainId, contractName);
  const contractABI = getContractABI(contractName);

  const web3 = getWeb3Obj();

  return new web3.eth.Contract(contractABI, contractAddress);
}
