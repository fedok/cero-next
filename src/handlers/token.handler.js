import { modalClose, modalSetActiveWindow } from 'redux/reducers/src/modal/action';
import { contractAddFightLog, contractAddToken } from 'redux/reducers/src/contract/action';
import { getExperienceCountForLevelUp } from './math.handler';
// eslint-disable-next-line import/no-cycle
import { getCero, getFightLogs, getTokenOwner } from './contracts/view.handler';

export const TOKEN_CLASSES = {
  warrior: 'Warrior',
  thief: 'Thief',
  wizard: 'Wizard',
};

function getTokenClass(st, pr, ag, ma) {
  const warriorPoint = Number(st) + Number(pr);
  const thiefPoint = Number(st) + Number(ag);
  const wizardPoint = Number(ag) + Number(ma);

  if (warriorPoint >= thiefPoint && warriorPoint > wizardPoint) {
    return TOKEN_CLASSES.warrior;
  }
  if (thiefPoint >= warriorPoint && thiefPoint > wizardPoint) {
    return TOKEN_CLASSES.thief;
  }

  return TOKEN_CLASSES.wizard;
}

export async function findToken(
    store,
    dispatch,
    tokenNum,
    usePreloader = true,
    hardUpdate = false,
    tokenOwner = '',
    uploadOwnerIfNotExist = false,
) {
  const { tokens } = store.contract;
  let tokenToReturn = null;
  let updateStore = false;

  // Start preloader
  if (usePreloader) {
    dispatch(modalSetActiveWindow('modal-preloader'));
  }

  // Search token in store or upload from contract. If upload from contract, update store
  if (hardUpdate === false) {
    // eslint-disable-next-line no-restricted-syntax
    for (const token of tokens) {
      if (Number(token.id) === Number(tokenNum)) {
        tokenToReturn = token;
        break;
      }
    }
  }

  if (tokenToReturn === null) {
    const newToken = await getCero(tokenNum, tokenOwner);
    if (newToken === null) return null;

    updateStore = true;
    tokenToReturn = newToken;
  }

  // Set token owner if needed and token owner not set, update store if need
  if (uploadOwnerIfNotExist && tokenToReturn.owner.length === 0) {
    const newTokenOwner = (await getTokenOwner(tokenNum)).toLowerCase();

    updateStore = true;
    tokenToReturn = { ...tokenToReturn, owner: newTokenOwner };
  }

  // Update store if needed
  if (updateStore) {
    dispatch(contractAddToken(tokenToReturn));
  }

  // Stop preloader
  if (usePreloader) {
    dispatch(modalClose);
  }

  return tokenToReturn;
}

export async function findFightLogs(store, dispatch, tokenNum, usePreloader = true, hardUpdate = false) {
  const { fightLogs } = store.contract;

  if (hardUpdate === false) {
    if (fightLogs[tokenNum] !== undefined) {
      return fightLogs[tokenNum];
    }
  }

  if (usePreloader) {
    dispatch(modalSetActiveWindow('modal-preloader'));
  }

  const logs = await getFightLogs(tokenNum);
  if (logs !== null) dispatch(contractAddFightLog(tokenNum, logs));

  if (usePreloader) {
    dispatch(modalClose);
  }

  return logs;
}

export function findAccountTokens(store, onlyActive = false) {
  const { tokens } = store.contract;
  const { account } = store.web3;
  const accountTokens = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const token of tokens) {
    if (token.owner === account) {
      if (onlyActive && token.isChild === false) {
        // eslint-disable-next-line no-continue
        continue;
      }
      accountTokens.push(token);
    }
  }

  return accountTokens;
}

export function tokenNumberToString(tokenNum) {
  let newTokenNum = String(tokenNum);
  for (let i = newTokenNum.length; i < 6; i += 1) {
    newTokenNum = `0${newTokenNum}`;
  }

  newTokenNum = `#${newTokenNum}`;

  return newTokenNum;
}

export function getTokenImageByClassAndNum(num, tokenClass) {
  switch (tokenClass) {
    case TOKEN_CLASSES.warrior:
      return `/images/ceroes/warrior/${(num % 8) + 1}.svg`;
    case TOKEN_CLASSES.thief:
      return `/images/ceroes/thief/${(num % 7) + 1}.svg`;
    default:
      return `/images/ceroes/wizard/${(num % 7) + 1}.svg`;
  }
}

export function getTokenHealth(level, strength) {
  return process.env.baseHP * level + strength * process.env.baseHPIncrease;
}

export function getTokenDescription(tokenClass) {
  switch (tokenClass) {
    case TOKEN_CLASSES.warrior:
      // eslint-disable-next-line max-len
      return 'A warrior is a professional fighter: a soldier, a mercenary, a knight, even yesterday\'s workshop craftsman, who needed swordsmanship skills only in case of inter-shop showdowns. What distinguishes a warrior from a mere dubolok is a high skill in handling weapons, a decent outfit and an emphasis not only on strength and speed, but also on combat techniques and tactical thinking.';
    case TOKEN_CLASSES.thief:
      // eslint-disable-next-line max-len
      return 'The thief is a nimble hero who prefers not to get involved in melee combat. Considering that shooting arrows or throwing shurikens is quite boring, in more modern worlds rangers are equipped with traps, explosives, various types of projectiles. Morale is usually neutral, and in general - the type is rather unsociable, but not malicious.';
    default:
      // eslint-disable-next-line max-len
      return 'The traditional image of the magician is a robe to his heels, a staff or wand, and a long beard. The standard fantasy mage is a respectable person. Moral analogue of a scholar using some sort of impersonal power for his own benefit and/or the benefit of those around him. A very common characteristic of the class is that at high levels it gains immense power.';
  }
}

export function transferSolTokenToProjectToken(solToken, id, tokenOwner) {
  const tokenClass = getTokenClass(solToken.strength, solToken.protection, solToken.agility, solToken.magic);

  return {
    id: Number(id),
    name: solToken.name,
    level: Number(solToken.lvl),
    exp: Number(solToken.experience),
    expToUp: getExperienceCountForLevelUp(Number(solToken.lvl) + 1),
    class: tokenClass,
    img: getTokenImageByClassAndNum(id, tokenClass),
    strength: Number(solToken.strength),
    protection: Number(solToken.protection),
    agility: Number(solToken.agility),
    magic: Number(solToken.magic),
    health: getTokenHealth(solToken.lvl, solToken.strength),
    isChild: solToken.isChild,
    parent1: Number(solToken.parent1),
    parent2: Number(solToken.parent2),
    birthday: Number(solToken.birthday),
    description: getTokenDescription(tokenClass),
    owner: tokenOwner.toLowerCase(),
  };
}

export function transferSolTokensToProjectTokens(tokensInfo, tokenOwner = '') {
  if (tokensInfo[0].length === 0) return [];

  const newTokens = [];
  tokensInfo[0].forEach((token, key) => {
    let id;
    if (tokensInfo[1] !== undefined) {
      id = Number(tokensInfo[1][key]);
    } else {
      id = key;
    }
    const newToken = transferSolTokenToProjectToken(token, id, tokenOwner);

    newTokens.push(newToken);
  });

  return newTokens;
}
