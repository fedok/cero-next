const months = [
  'Jan',
  'Feb',
  'Ma',
  'Apr',
  'May',
  'June',
  'July',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec',
];

const monthsRu = [
  'Янв',
  'Фев',
  'Мар',
  'Апр',
  'Май',
  'Июн',
  'Июл',
  'Авг',
  'Сен',
  'Окт',
  'Ноя',
  'Дек',
];

function parseDate(solTimestamp, lang) {
  const date = new Date(solTimestamp * 1000);

  const dateNum = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  const monthFull1 = lang === 'en' ? months[date.getMonth()] : monthsRu[date.getMonth()];
  const year = date.getFullYear();
  const hours = (`0${date.getHours()}`).slice(-2);
  const minutes = (`0${date.getMinutes()}`).slice(-2);

  return { dateNum, month, monthFull1, year, hours, minutes };
}

export function fromSolDateToDateFormatType1(solTimestamp, lang = 'en') {
  if (Number(solTimestamp) === 0) return '0';

  const data = parseDate(solTimestamp, lang);
  return `${data.dateNum} ${data.monthFull1} ${data.year}, ${data.hours}:${data.minutes}`;
}
