import { faqEn, modalsEn, landingEn, headerEn } from 'handlers/translator/src/en';
import {
  collectionRu, commonRu,
  creationRu, faqRu,
  headerRu, landingRu,
  levelUpRu,
  modalsRu, ratingRu,
  tokenInfoRu,
  tokenRu,
} from 'handlers/translator/src/ru';

export default class Translator {
  constructor(language, section) {
    this.language = language;
    this.section = section;

    let obj = {};
    if (this.language === 'ru') {
      switch (this.section) {
        case 'creation':
          obj = creationRu;
          break;
        case 'modals':
          obj = modalsRu;
          break;
        case 'levelUp':
          obj = levelUpRu;
          break;
        case 'token':
          obj = tokenRu;
          break;
        case 'tokenInfo':
          obj = tokenInfoRu;
          break;
        case 'header':
          obj = headerRu;
          break;
        case 'collection':
          obj = collectionRu;
          break;
        case 'rating':
          obj = ratingRu;
          break;
        case 'faq':
          obj = faqRu;
          break;
        case 'landing':
          obj = landingRu;
          break;
        default:
          obj = commonRu;
      }
    } else {
      switch (this.section) {
        case 'modals':
          obj = modalsEn;
          break;
        case 'header':
          obj = headerEn;
          break;
        case 'faq':
          obj = faqEn;
          break;
        case 'landing':
          obj = landingEn;
          break;
        default:
          obj = {};
      }
    }

    this.obj = obj;
  }

  t(message) {
    if (Object.prototype.hasOwnProperty.call(this.obj, message)) {
      return this.obj[message];
    }
    return message;
  }
}
